﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesReport.Class
{
    public class WriteData
    {
        public DataTable Datasource { get; set; }
        public string SheetName { get; set; }
        public int StartColumn { get; set; }
        public int StartRow { get; set; }

        public string SaveFileName { get; set; }
        public bool IsSaveFile { get; set; }
    }
}
