﻿namespace SalesReport
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btSendMail = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.salesReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tiniPassSalesReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.billAndTrafficReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.volumeAnalyticsReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.topUpCampaignReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.marketingOnlineSaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btSendMail
            // 
            this.btSendMail.ContextMenuStrip = this.contextMenuStrip1;
            this.btSendMail.Location = new System.Drawing.Point(12, 31);
            this.btSendMail.Name = "btSendMail";
            this.btSendMail.Size = new System.Drawing.Size(236, 90);
            this.btSendMail.TabIndex = 0;
            this.btSendMail.Text = "Resend Mail Report";
            this.btSendMail.UseVisualStyleBackColor = true;
            this.btSendMail.Click += new System.EventHandler(this.btSendMail_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripSeparator1,
            this.toolStripMenuItem2,
            this.toolStripSeparator2,
            this.salesReportToolStripMenuItem,
            this.toolStripSeparator3,
            this.tiniPassSalesReportToolStripMenuItem,
            this.toolStripSeparator4,
            this.billAndTrafficReportToolStripMenuItem,
            this.toolStripSeparator5,
            this.volumeAnalyticsReportToolStripMenuItem,
            this.toolStripSeparator6,
            this.topUpCampaignReportToolStripMenuItem,
            this.toolStripSeparator7,
            this.marketingOnlineSaleToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(329, 335);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(328, 32);
            this.toolStripMenuItem1.Text = "Job All TiniPass Transactions";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(325, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(328, 32);
            this.toolStripMenuItem2.Text = "Trending Revenue Report";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(325, 6);
            // 
            // salesReportToolStripMenuItem
            // 
            this.salesReportToolStripMenuItem.Name = "salesReportToolStripMenuItem";
            this.salesReportToolStripMenuItem.Size = new System.Drawing.Size(328, 32);
            this.salesReportToolStripMenuItem.Text = "Sales Report";
            this.salesReportToolStripMenuItem.Click += new System.EventHandler(this.salesReportToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(325, 6);
            // 
            // tiniPassSalesReportToolStripMenuItem
            // 
            this.tiniPassSalesReportToolStripMenuItem.Name = "tiniPassSalesReportToolStripMenuItem";
            this.tiniPassSalesReportToolStripMenuItem.Size = new System.Drawing.Size(328, 32);
            this.tiniPassSalesReportToolStripMenuItem.Text = "TiniPass Sales Report";
            this.tiniPassSalesReportToolStripMenuItem.Click += new System.EventHandler(this.tiniPassSalesReportToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(325, 6);
            // 
            // billAndTrafficReportToolStripMenuItem
            // 
            this.billAndTrafficReportToolStripMenuItem.Name = "billAndTrafficReportToolStripMenuItem";
            this.billAndTrafficReportToolStripMenuItem.Size = new System.Drawing.Size(328, 32);
            this.billAndTrafficReportToolStripMenuItem.Text = "Bill And Traffic Report";
            this.billAndTrafficReportToolStripMenuItem.Click += new System.EventHandler(this.billAndTrafficReportToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(325, 6);
            // 
            // volumeAnalyticsReportToolStripMenuItem
            // 
            this.volumeAnalyticsReportToolStripMenuItem.Name = "volumeAnalyticsReportToolStripMenuItem";
            this.volumeAnalyticsReportToolStripMenuItem.Size = new System.Drawing.Size(328, 32);
            this.volumeAnalyticsReportToolStripMenuItem.Text = "Volume Analytics Report";
            this.volumeAnalyticsReportToolStripMenuItem.Click += new System.EventHandler(this.volumeAnalyticsReportToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(325, 6);
            // 
            // topUpCampaignReportToolStripMenuItem
            // 
            this.topUpCampaignReportToolStripMenuItem.Name = "topUpCampaignReportToolStripMenuItem";
            this.topUpCampaignReportToolStripMenuItem.Size = new System.Drawing.Size(328, 32);
            this.topUpCampaignReportToolStripMenuItem.Text = "TopUp Campaign Report";
            this.topUpCampaignReportToolStripMenuItem.Click += new System.EventHandler(this.topUpCampaignReportToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(325, 6);
            // 
            // marketingOnlineSaleToolStripMenuItem
            // 
            this.marketingOnlineSaleToolStripMenuItem.Name = "marketingOnlineSaleToolStripMenuItem";
            this.marketingOnlineSaleToolStripMenuItem.Size = new System.Drawing.Size(328, 32);
            this.marketingOnlineSaleToolStripMenuItem.Text = "Marketing Online Sale";
            this.marketingOnlineSaleToolStripMenuItem.Click += new System.EventHandler(this.marketingOnlineSaleToolStripMenuItem_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 6000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "Sales Report";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Sales Report";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(267, 31);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(214, 90);
            this.button1.TabIndex = 1;
            this.button1.Text = "CRMDATA JSON";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 151);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btSendMail);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SendMail";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btSendMail;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem salesReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tiniPassSalesReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billAndTrafficReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem volumeAnalyticsReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem topUpCampaignReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem marketingOnlineSaleToolStripMenuItem;
    }
}