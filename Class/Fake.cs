﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesReport
{
    public class Fake
    {
        public string timestamp { get; set; }
        public string type { get; set; }
        public int? id { get; set; }
        public int? previousEventId { get; set; }
        public Fake2 stateEnteredEventDetails { get; set; } = new Fake2();
        public string executionArn { get; set; }
    }
    public class Fake2
    {
        public string name { get; set; }
        public string input { get; set; }
    }
}
