﻿using SalesReport.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SalesReport
{
    public class ExcelUtilities
    {
        #region Source
        private string _strSourcePath;
        public string strSourcePath { get => _strSourcePath; set { _strSourcePath = value; strSourceFullDirect = string.Format("{0}\\{1}", _strSourcePath, _strSourceFileName); } }

        private string _strSourceFileName;
        private string _strSourceFileNameATT;
        private string _strSourceFileNameDIS;
        private string _strSourceFileNameRET;
        public string strSourceFileName { get => _strSourceFileName; set { _strSourceFileName = value; strSourceFullDirect = string.Format("{0}\\{1}", _strSourcePath, _strSourceFileName); } }
        public string strSourceFileNameATT { get => _strSourceFileNameATT; set { _strSourceFileNameATT = value; strSourceFullDirectATT = string.Format("{0}\\{1}", _strSourcePath, _strSourceFileNameATT); } }
        public string strSourceFileNameRET { get => _strSourceFileNameRET; set { _strSourceFileNameRET = value; strSourceFullDirectRET = string.Format("{0}\\{1}", _strSourcePath, _strSourceFileNameRET); } }
        public string strSourceFileNameDIS { get => _strSourceFileNameDIS; set { _strSourceFileNameDIS = value; strSourceFullDirectDIS = string.Format("{0}\\{1}", _strSourcePath, _strSourceFileNameDIS); } }

        public string strSourceFullDirect { get; set; }
        public string strSourceFullDirectATT { get; set; }
        public string strSourceFullDirectRET { get; set; }
        public string strSourceFullDirectDIS { get; set; }
        #endregion

        #region Destination
        private string _strDesPath;
        public string strDesPath { get => _strDesPath; set { _strDesPath = value; strDesFullDirect = string.Format("{0}\\{1}", _strDesPath, _strDesFileName); } }

        private string _strDesFileName;
        public string strDesFileName { get => _strDesFileName; set { _strDesFileName = value; strDesFullDirect = string.Format("{0}\\{1}", _strDesPath, _strDesFileName); } }

        public string strDesFullDirect { get; set; }
        public string strDesDisplaySheet { get; set; }
        #endregion


        public void WriteData(DataSet dataSet, string[] sheetArr, string saveAsLocation, int startCol, int startRow, DateTime dToDate)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(strSourceFullDirect);

                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets["Ghi chú"];
                xlWorksheet.Cells[1, 1] = dToDate.ToString("MM/dd/yyyy"); 
                releaseObject(xlWorksheet);
                for (int i = 0; i < sheetArr.Length; i++)
                {
                    Console.WriteLine($"Removing Sheet [{sheetArr[i]}]");
                    xlWorksheet = xlWorkbook.Sheets[sheetArr[i]];

                    for (int j = 0; j < dataSet.Tables[i].Rows.Count; j++)
                    {
                        Console.WriteLine($"Sheet [{sheetArr[i]}] row index: " + j);
                        for (int k = 0; k < dataSet.Tables[i].Columns.Count; k++)
                        {
                            var obj = dataSet.Tables[i].Rows[j][k];
                            xlWorksheet.Cells[j + startRow, startCol + k] = obj.ToString();
                        }
                    }
                    releaseObject(xlWorksheet);
                }
                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //rule of thumb for releasing com objects:  
                //  never use two dots, all COM objects must be referenced and released individually  
                //  ex: [somthing].[something].[something] is bad  
                //xlWorkbook.Save();
                xlWorkbook.RefreshAll();

                xlApp.Application.CalculateFull();

                xlWorkbook.SaveAs(saveAsLocation, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange
                    , Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlWorkbook);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetFilter()
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(strSourceFullDirect);
                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets["PrivateSales"];
                Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;
                object misValue = System.Reflection.Missing.Value;

                int rowCount = xlRange.Rows.Count;
                int colCount = xlRange.Columns.Count;

                // dt.Column = colCount;  
                if (colCount < 1)
                    throw new Exception("No data");

                xlWorksheet.EnableAutoFilter = true;
                var cell = xlWorksheet.Cells["1", "B"];
                cell.AutoFilter(1);
                //xlWorksheet.Cells["2", "G"].Formula = "=SUM(G5:G1048576)";
                //xlWorksheet.Cells["2", "H"].Formula = "=SUM(H5:H1048576)";
                //xlWorksheet.Cells["2", "I"].Formula = "=SUM(I5:I1048576)";
                //xlWorksheet.Cells["2", "J"].Formula = "=SUM(J5:J1048576)";
                //xlWorksheet.Cells["2", "K"].Formula = "=SUM(K5:K1048576)";
                //xlWorksheet.Cells["2", "L"].Formula = "=SUM(L5:L1048576)";
                //xlWorksheet.Cells["2", "M"].Formula = "=SUM(M5:M1048576)";
                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //rule of thumb for releasing com objects:  
                //  never use two dots, all COM objects must be referenced and released individually  
                //  ex: [somthing].[something].[something] is bad  
                xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlRange);
                releaseObject(xlWorksheet);
                releaseObject(xlWorkbook);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void KillSpecificExcelFileProcess(string Path)
        {
            try
            {
               
                if( Path!="")
                {
                    string excelFileName = System.IO.Path.GetFileNameWithoutExtension(Path);



                    string ProcessName = "EXCEL";//args[0];

                    ProcessHelper ph = new ProcessHelper();

                    ph.KillProcessByNameAndUser(ProcessName, excelFileName);
                }
                


            }
            catch
            {
              
                KillSpecificExcelFileProcess(Path);
            }
        }
        public void KillSpecificExcelFileProcessBackgroud()
        {
            try
            {

                string ProcessName = "EXCEL";//args[0];

                ProcessHelper ph = new ProcessHelper();

                ph.KillProcessByNameAndUserBackgroud(ProcessName);

            }
            catch
            {

                KillSpecificExcelFileProcessBackgroud();
            }
        }
        private void CloseExcelWorkbook(string workbookName)
        {
            try
            {
                // Dim plist As Process() = Process.GetProcessesByName("Excel", ".")

                // If plist.Length > 1 Then
                // 'Throw New Exception("More than one Excel process running.")
                // ElseIf plist.Length = 0 Then
                // Throw New Exception("No Excel process running.")
                // End If

                object obj = Marshal.GetActiveObject("Excel.Application");
                Microsoft.Office.Interop.Excel.Application excelAppl = (Microsoft.Office.Interop.Excel.Application)obj;
                Microsoft.Office.Interop.Excel.Workbooks workbooks = excelAppl.Workbooks;

                foreach (Microsoft.Office.Interop.Excel.Workbook wkbk in workbooks)
                {
                    if (wkbk.Name == workbookName)
                    {
                        Marshal.ReleaseComObject(wkbk.Worksheets);
                        wkbk.Close();

                        // excelAppl.Quit()
                        Marshal.ReleaseComObject(wkbk);
                    }
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                if (workbooks != null)
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbooks);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excelAppl);
                GC.Collect();
            }
            catch (Exception ex)
            {
            }
        }
        public void WriteData1(System.Data.DataTable dataTable, string strSheetName, int startCol, int startRow,string fileName,bool isSaveToSource)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"\Retail_Revenue_Report.xlsx"); //strDesFullDirect
                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[strSheetName];
                Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;
                object misValue = System.Reflection.Missing.Value;

                int col, row;
                object[,] rawData = new object[dataTable.Rows.Count + 1, dataTable.Columns.Count - 1 + 1];

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                    rawData[0, col] = dataTable.Columns[col].ColumnName;

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                {
                    for (row = 0; row <= dataTable.Rows.Count - 1; row++)
                        rawData[row + 1, col] = dataTable.Rows[row].ItemArray[col];
                }
                string finalColLetter = ExcelColumnName(dataTable.Columns.Count);
                string excelRange = string.Format("A1:{0}{1}", finalColLetter, dataTable.Rows.Count + 1);
                xlWorksheet.Range[excelRange, Type.Missing].Value2 = rawData;

                //for (int i = 0; i < dataTable.Rows.Count; i++)
                //{
                //    for (int j = 0; j < dataTable.Columns.Count; j++)
                //    {
                //        var obj = dataTable.Rows[i][j];
                //        xlWorksheet.Cells[i + startRow, startCol + j] = obj.ToString();
                //    }
                //}


                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();




               

                if (isSaveToSource == true)
                {
                    xlWorkbook.RefreshAll();

                    xlApp.Application.CalculateFull();

                    xlWorksheet.Cells[1, dataTable.Columns.Count+1] ="Last update time: "+ DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    xlWorkbook.SaveAs(fileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                }
                else
                    xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlRange);
                releaseObject(xlWorksheet);
                releaseObject(xlWorkbook);
            }
            catch(Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                WriteData1( dataTable,  strSheetName,  startCol, startRow, fileName,  isSaveToSource);
            }
        }
        public void WriteDataTiniPass(System.Data.DataTable dataTable, string strSheetName, int startCol, int startRow, string fileName, bool isSaveToSource)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"\TiniPass_Revenue_Report.xlsx"); //strDesFullDirect
                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[strSheetName];
                Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;
                object misValue = System.Reflection.Missing.Value;

                int col, row;
                object[,] rawData = new object[dataTable.Rows.Count + 1, dataTable.Columns.Count - 1 + 1];

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                    rawData[0, col] = dataTable.Columns[col].ColumnName;

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                {
                    for (row = 0; row <= dataTable.Rows.Count - 1; row++)
                        rawData[row + 1, col] = dataTable.Rows[row].ItemArray[col];
                }
                string finalColLetter = ExcelColumnName(dataTable.Columns.Count);
                string excelRange = string.Format("A1:{0}{1}", finalColLetter, dataTable.Rows.Count + 1);
                xlWorksheet.Range[excelRange, Type.Missing].Value2 = rawData;

                //for (int i = 0; i < dataTable.Rows.Count; i++)
                //{
                //    for (int j = 0; j < dataTable.Columns.Count; j++)
                //    {
                //        var obj = dataTable.Rows[i][j];
                //        xlWorksheet.Cells[i + startRow, startCol + j] = obj.ToString();
                //    }
                //}


                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();




               

                if (isSaveToSource == true)
                {
                    xlWorkbook.RefreshAll();

                    xlApp.Application.CalculateFull();

                    xlWorksheet.Cells[1, dataTable.Columns.Count + 1] = "Last update time: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    xlWorkbook.SaveAs(fileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                }
                else
                    xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlRange);
                releaseObject(xlWorksheet);
                releaseObject(xlWorkbook);
            }
            catch (Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                WriteDataTiniPass(dataTable, strSheetName, startCol, startRow, fileName, isSaveToSource);
            }
        }
        public void WriteDataVolumeAnalytics(System.Data.DataTable dataTable, string strSheetName, int startCol, int startRow, string fileName, bool isSaveToSource)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"\Volume_Analyse_Report.xlsx"); //strDesFullDirect
                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[strSheetName];
                Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;
                object misValue = System.Reflection.Missing.Value;

                int col, row;
                object[,] rawData = new object[dataTable.Rows.Count + 1, dataTable.Columns.Count - 1 + 1];

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                    rawData[0, col] = dataTable.Columns[col].ColumnName;

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                {
                    for (row = 0; row <= dataTable.Rows.Count - 1; row++)
                        rawData[row + 1, col] = dataTable.Rows[row].ItemArray[col];
                }
                string finalColLetter = ExcelColumnName(dataTable.Columns.Count);
                string excelRange = string.Format("A1:{0}{1}", finalColLetter, dataTable.Rows.Count + 1);
                xlWorksheet.Range[excelRange, Type.Missing].Value2 = rawData;

                //for (int i = 0; i < dataTable.Rows.Count; i++)
                //{
                //    for (int j = 0; j < dataTable.Columns.Count; j++)
                //    {
                //        var obj = dataTable.Rows[i][j];
                //        xlWorksheet.Cells[i + startRow, startCol + j] = obj.ToString();
                //    }
                //}


                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();




                

                if (isSaveToSource == true)
                {
                    xlWorkbook.RefreshAll();

                    xlApp.Application.CalculateFull();

                    xlWorksheet.Cells[1, dataTable.Columns.Count + 1] = "Last update time: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    xlWorkbook.SaveAs(fileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                }
                else
                    xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlRange);
                releaseObject(xlWorksheet);
                releaseObject(xlWorkbook);
            }
            catch (Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                WriteDataVolumeAnalytics(dataTable, strSheetName, startCol, startRow, fileName, isSaveToSource);
            }
        }
        public void WriteDataToUpCampaign(System.Data.DataTable dataTable, string strSheetName, int startCol, int startRow, string fileName, bool isSaveToSource)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"\TopUp_Campaign_Template.xlsx"); //strDesFullDirect
                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[strSheetName];
                Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;
                object misValue = System.Reflection.Missing.Value;

                int col, row;
                object[,] rawData = new object[dataTable.Rows.Count + 1, dataTable.Columns.Count - 1 + 1];

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                    rawData[0, col] = dataTable.Columns[col].ColumnName;

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                {
                    for (row = 0; row <= dataTable.Rows.Count - 1; row++)
                        rawData[row + 1, col] = dataTable.Rows[row].ItemArray[col];
                }
                string finalColLetter = ExcelColumnName(dataTable.Columns.Count);
                string excelRange = string.Format("A1:{0}{1}", finalColLetter, dataTable.Rows.Count + 1);
                xlWorksheet.Range[excelRange, Type.Missing].Value2 = rawData;

                //for (int i = 0; i < dataTable.Rows.Count; i++)
                //{
                //    for (int j = 0; j < dataTable.Columns.Count; j++)
                //    {
                //        var obj = dataTable.Rows[i][j];
                //        xlWorksheet.Cells[i + startRow, startCol + j] = obj.ToString();
                //    }
                //}


                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();




               
                if (isSaveToSource == true)
                {
                    xlWorkbook.RefreshAll();

                    xlApp.Application.CalculateFull();


                    xlWorksheet.Cells[1, dataTable.Columns.Count + 1] = "Last update time: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    xlWorkbook.SaveAs(fileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                }
                else
                    xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlRange);
                releaseObject(xlWorksheet);
                releaseObject(xlWorkbook);
            }
            catch (Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                WriteDataToUpCampaign(dataTable, strSheetName, startCol, startRow, fileName, isSaveToSource);
            }
        }

        public void WriteDataTrendingRevenueDaily(System.Data.DataTable dataTable, string strSheetName, int startCol, int startRow, string fileName, bool isSaveToSource)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"\Trending_Revenue_Daily_Template.xlsx"); //strDesFullDirect
                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[strSheetName];
                Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;
                object misValue = System.Reflection.Missing.Value;

                int col, row;
                object[,] rawData = new object[dataTable.Rows.Count + 1, dataTable.Columns.Count - 1 + 1];

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                    rawData[0, col] = dataTable.Columns[col].ColumnName;

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                {
                    for (row = 0; row <= dataTable.Rows.Count - 1; row++)
                        rawData[row + 1, col] = dataTable.Rows[row].ItemArray[col];
                }
                string finalColLetter = ExcelColumnName(dataTable.Columns.Count);
                string excelRange = string.Format("A1:{0}{1}", finalColLetter, dataTable.Rows.Count + 1);
                xlWorksheet.Range[excelRange, Type.Missing].Value2 = rawData;

                //for (int i = 0; i < dataTable.Rows.Count; i++)
                //{
                //    for (int j = 0; j < dataTable.Columns.Count; j++)
                //    {
                //        var obj = dataTable.Rows[i][j];
                //        xlWorksheet.Cells[i + startRow, startCol + j] = obj.ToString();
                //    }
                //}


                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();




               


                if (isSaveToSource == true)
                {
                    xlWorkbook.RefreshAll();

                    xlApp.Application.CalculateFull();
                    xlWorksheet.Cells[1, dataTable.Columns.Count + 1] = "Last update time: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    xlWorkbook.SaveAs(fileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                }
                else
                    xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlRange);
                releaseObject(xlWorksheet);
                releaseObject(xlWorkbook);
            }
            catch (Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                WriteDataTrendingRevenueDaily(dataTable, strSheetName, startCol, startRow, fileName, isSaveToSource);
            }
        }

        public void WriteDataBillAndTraffic(System.Data.DataTable dataTable, string strSheetName, int startCol, int startRow, string fileName, bool isSaveToSource)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"\Traffic_And_Bill_Report.xlsx"); //strDesFullDirect
                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[strSheetName];
                Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;
                object misValue = System.Reflection.Missing.Value;

                int col, row;
                object[,] rawData = new object[dataTable.Rows.Count + 1, dataTable.Columns.Count - 1 + 1];

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                    rawData[0, col] = dataTable.Columns[col].ColumnName;

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                {
                    for (row = 0; row <= dataTable.Rows.Count - 1; row++)
                        rawData[row + 1, col] = dataTable.Rows[row].ItemArray[col];
                }
                string finalColLetter = ExcelColumnName(dataTable.Columns.Count);
                string excelRange = string.Format("A1:{0}{1}", finalColLetter, dataTable.Rows.Count + 1);
                xlWorksheet.Range[excelRange, Type.Missing].Value2 = rawData;

                //for (int i = 0; i < dataTable.Rows.Count; i++)
                //{
                //    for (int j = 0; j < dataTable.Columns.Count; j++)
                //    {
                //        var obj = dataTable.Rows[i][j];
                //        xlWorksheet.Cells[i + startRow, startCol + j] = obj.ToString();
                //    }
                //}


                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();




                

                if (isSaveToSource == true)
                {
                    xlWorkbook.RefreshAll();

                    xlApp.Application.CalculateFull();

                    xlWorksheet.Cells[1, dataTable.Columns.Count + 1] = "Last update time: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    xlWorkbook.SaveAs(fileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                }
                else
                    xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlRange);
                releaseObject(xlWorksheet);
                releaseObject(xlWorkbook);
            }
            catch (Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                WriteDataBillAndTraffic(dataTable, strSheetName, startCol, startRow, fileName, isSaveToSource);
            }
        }

        public void WriteDataRange(System.Data.DataTable dataTable, string strSheetName, int startCol, int startRow, string sourcefileName, string fileName, bool isSaveToSource)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(sourcefileName); //strDesFullDirect
                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[strSheetName];
                Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;
                object misValue = System.Reflection.Missing.Value;

                int col, row;
                object[,] rawData = new object[dataTable.Rows.Count + 1, dataTable.Columns.Count - 1 + 1];

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                    rawData[0, col] = dataTable.Columns[col].ColumnName;

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                {
                    for (row = 0; row <= dataTable.Rows.Count - 1; row++)
                        rawData[row + 1, col] = dataTable.Rows[row].ItemArray[col];
                }
                string finalColLetter = ExcelColumnName(dataTable.Columns.Count);
                string excelRange = string.Format("A1:{0}{1}", finalColLetter, dataTable.Rows.Count + 1);
                xlWorksheet.Range[excelRange, Type.Missing].Value2 = rawData;

                //for (int i = 0; i < dataTable.Rows.Count; i++)
                //{
                //    for (int j = 0; j < dataTable.Columns.Count; j++)
                //    {
                //        var obj = dataTable.Rows[i][j];
                //        xlWorksheet.Cells[i + startRow, startCol + j] = obj.ToString();
                //    }
                //}


                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();




              

                if (isSaveToSource == true)
                {
                    xlWorkbook.RefreshAll();

                    xlApp.Application.CalculateFull();

                    string file_csv = AppDomain.CurrentDomain.BaseDirectory + @"\Temp\Haravan_Inventory.csv";
                    file_csv = file_csv.Replace("\\\\", "\\");
                    
                    xlWorkbook.SaveAs(fileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    
                    xlWorkbook.SaveAs(file_csv, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSV, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                }
                else
                    xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlRange);
                releaseObject(xlWorksheet);
                releaseObject(xlWorkbook);
            }
            catch (Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                WriteDataRange(dataTable, strSheetName, startCol, startRow,sourcefileName, fileName, isSaveToSource);
            }
        }
        public void WriteDataStockOnHand(System.Data.DataTable dataTable, string strSheetName, int startCol, int startRow, string fileName)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"\Daily_StockOnHand.xlsx"); //strDesFullDirect
                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[strSheetName];
                Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;
                object misValue = System.Reflection.Missing.Value;

                int col, row;
                object[,] rawData = new object[dataTable.Rows.Count + 1, dataTable.Columns.Count - 1 + 1];

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                    rawData[0, col] = dataTable.Columns[col].ColumnName;

                for (col = 0; col <= dataTable.Columns.Count - 1; col++)
                {
                    for (row = 0; row <= dataTable.Rows.Count - 1; row++)
                        rawData[row + 1, col] = dataTable.Rows[row].ItemArray[col];
                }
                string finalColLetter = ExcelColumnName(dataTable.Columns.Count);
                string excelRange = string.Format("A1:{0}{1}", finalColLetter, dataTable.Rows.Count + 1);
                xlWorksheet.Range[excelRange, Type.Missing].Value2 = rawData;

                //for (int i = 0; i < dataTable.Rows.Count; i++)
                //{
                //    for (int j = 0; j < dataTable.Columns.Count; j++)
                //    {
                //        var obj = dataTable.Rows[i][j];
                //        xlWorksheet.Cells[i + startRow, startCol + j] = obj.ToString();
                //    }
                //}


                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();




                xlWorkbook.RefreshAll();

                xlApp.Application.CalculateFull();

                 xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlRange);
                releaseObject(xlWorksheet);
                releaseObject(xlWorkbook);
            }
            catch (Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                WriteDataStockOnHand(dataTable, strSheetName, startCol, startRow, fileName);
            }
        }

        private string ExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }
        public void CopyRange(string strSheetName, string strDesSheetName)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(strDesFullDirect);
                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[strSheetName];
                Microsoft.Office.Interop.Excel._Worksheet xlDesWorksheet = xlWorkbook.Sheets[strDesSheetName];
                Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;
                object misValue = System.Reflection.Missing.Value;

                int rowCount = xlRange.Rows.Count;
                var from = xlWorksheet.Range["A3", "C" + rowCount];
                var to = xlDesWorksheet.Range["D40", "F" + (40 + rowCount)];
                from.Copy(to);
                var xlFormatRange = xlDesWorksheet.Range["E42", "E100"];
                xlFormatRange.NumberFormat = "dd/MM/yyyy";
                //to.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats);
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //rule of thumb for releasing com objects:  
                //  never use two dots, all COM objects must be referenced and released individually  
                //  ex: [somthing].[something].[something] is bad  
                xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlRange);
                releaseObject(xlWorksheet);
                releaseObject(xlWorkbook);
            }
            catch { }
        }

        public void RemoveRange(List<string> lsSheetRawData, string fileName,int StartRow=1,int StartColumn=1)
        {
            try
            {
                KillSpecificExcelFileProcess(fileName);
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(fileName);

                foreach (string SheetRawData in lsSheetRawData)
                {
                    Console.WriteLine($"Removing Data In Sheet [{SheetRawData}]");

                    if (xlWorkbook.Sheets[SheetRawData] != null)
                    {
                        Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[SheetRawData];
                        //Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;

                        string startColLetter = ExcelColumnName(StartColumn);
                        string finalColLetter = ExcelColumnName(200);
                        string excelRange = string.Format(startColLetter + StartRow.ToString() + ":{0}{1}", finalColLetter, 1000000);
                        xlWorksheet.Range[excelRange, Type.Missing].Cells.ClearContents();



                        //int rowCount = xlRange.Rows.Count;
                        //int colCount = xlRange.Columns.Count;

                        //if (rowCount >= 1 && colCount >= 1)
                        //    xlRange.Cells.ClearContents();


                        //xlRange.Cells.ClearContents();
                        ////string strStartRange = string.Format("A{0}", startRange);
                        ////string strLastRange = string.Format("AZ{0}", rowCount.ToString());

                        ////var xlRemoveRange = xlWorksheet.Range[strStartRange, strLastRange];
                        ////xlRemoveRange.Cells.Delete();

                        //releaseObject(xlRange);
                        releaseObject(xlWorksheet);
                    }

                }



                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();

                xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlWorkbook);
            }
            catch
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                RemoveRange(lsSheetRawData, fileName);
            }
        }
        public void HideRangeSheet(string sourceFile, List<string> lsSheetName)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(sourceFile);

                foreach (string strSheetName in lsSheetName)
                {
                    if (xlWorkbook.Sheets[strSheetName] != null)
                    {
                        xlWorkbook.Sheets[strSheetName].Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetVeryHidden;
                        releaseObject(xlWorkbook.Sheets[strSheetName]);
                    }
                }


                //foreach (Microsoft.Office.Interop.Excel.Worksheet xlworksheet in xlWorkbook.Worksheets)
                //{

                //    if (xlworksheet.Name == strSheetName)
                //    {
                //        xlworksheet.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetVeryHidden;

                //        releaseObject(xlworksheet);
                //    }

                //}
                ////rule of thumb for releasing com objects:  
                ////  never use two dots, all COM objects must be referenced and released individually  
                ////  ex: [somthing].[something].[something] is bad  

                ////cleanup  

                GC.Collect();
                GC.WaitForPendingFinalizers();

                xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlWorkbook);
            }
            catch (Exception ex)
            {


            }
        }

        public void RemoveRange(List<string> lsSheetRawData)
        {
            try
            {
                KillSpecificExcelFileProcess(AppDomain.CurrentDomain.BaseDirectory + @"\Retail_Revenue_Report.xlsx");
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory+ @"\Retail_Revenue_Report.xlsx");
                
                foreach (string SheetRawData in lsSheetRawData)
                {
                    Console.WriteLine($"Removing Data In Sheet [{SheetRawData}]");

                    if (xlWorkbook.Sheets[SheetRawData] != null)
                    {
                        Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[SheetRawData];
                        Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;

                        int rowCount = xlRange.Rows.Count;
                        int colCount = xlRange.Columns.Count;

                        if (rowCount >= 1 && colCount >= 1)
                            xlRange.Cells.ClearContents();


                        xlRange.Cells.ClearContents();
                        //string strStartRange = string.Format("A{0}", startRange);
                        //string strLastRange = string.Format("AZ{0}", rowCount.ToString());

                        //var xlRemoveRange = xlWorksheet.Range[strStartRange, strLastRange];
                        //xlRemoveRange.Cells.Delete();
                        xlWorksheet.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetVeryHidden;
                        releaseObject(xlRange);
                        releaseObject(xlWorksheet);
                    }

                }



                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();

                xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlWorkbook);
            }
            catch
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                RemoveRange(lsSheetRawData);
            }
        }
        public void RemoveRangeStockOnHand(List<string> lsSheetRawData)
        {
            try
            {
                KillSpecificExcelFileProcess(AppDomain.CurrentDomain.BaseDirectory + @"\Daily_StockOnHand.xlsx");
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"\Daily_StockOnHand.xlsx");

                foreach (string SheetRawData in lsSheetRawData)
                {
                    Console.WriteLine($"Removing Data In Sheet [{SheetRawData}]");

                    if (xlWorkbook.Sheets[SheetRawData] != null)
                    {
                        Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[SheetRawData];
                        Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;

                        int rowCount = xlRange.Rows.Count;
                        int colCount = xlRange.Columns.Count;

                        if (rowCount >= 1 && colCount >= 1)
                            xlRange.Cells.ClearContents();


                        xlRange.Cells.ClearContents();
                        //string strStartRange = string.Format("A{0}", startRange);
                        //string strLastRange = string.Format("AZ{0}", rowCount.ToString());

                        //var xlRemoveRange = xlWorksheet.Range[strStartRange, strLastRange];
                        //xlRemoveRange.Cells.Delete();
          
                        releaseObject(xlRange);
                        releaseObject(xlWorksheet);
                    }

                }



                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();

                xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlWorkbook);
            }
            catch
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                RemoveRangeStockOnHand(lsSheetRawData);
            }
        }

        private static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        public DataTable ReadExcelFile(string strSheetName, int iStartCol, int iStartRow)
        {
            DataTable _dt;

            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(strSourceFullDirect);
                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[strSheetName];
                Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;

                int rowCount = xlRange.Rows.Count;
                int colCount = xlRange.Columns.Count;

                // dt.Column = colCount;  
                _dt = new DataTable();
                if (colCount < 1)
                    throw new Exception("No data");

                for (int col = iStartCol; col <= colCount; col++)//Header
                {
                    if (xlRange.Cells[iStartRow, col] == null || xlRange.Cells[iStartRow, col].Value2 == null)
                        break;

                    DataColumn dataColumn = new DataColumn();
                    dataColumn.ColumnName = xlRange.Cells[iStartRow, col].Value2;
                    _dt.Columns.Add(dataColumn);
                }

                for (int row = iStartRow + 1; row <= rowCount; row++)//Rows Values
                {
                    object[] array = new object[colCount];
                    List<object> list = new List<object>();
                    for (int col = iStartCol; col <= colCount; col++)
                    {
                        if (xlRange.Cells[row, col] == null || xlRange.Cells[row, col].Value2 == null)
                        {
                            col = iStartCol;
                            break;
                        }

                        list.Add(xlRange.Cells[row, col].Value2);
                    }
                    if (list.Count > 0)
                        _dt.Rows.Add(list.ToArray());
                }

                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //rule of thumb for releasing com objects:  
                //  never use two dots, all COM objects must be referenced and released individually  
                //  ex: [somthing].[something].[something] is bad  

                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlRange);
                releaseObject(xlWorksheet);
                releaseObject(xlWorkbook);
                return _dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public void HideSheet(string sourceFile, string strSheetName)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(sourceFile);

                foreach (Microsoft.Office.Interop.Excel.Worksheet xlworksheet in xlWorkbook.Worksheets)
                {

                    if (xlworksheet.Name == strSheetName)
                    {
                        xlworksheet.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetVeryHidden;
                      
                        releaseObject(xlworksheet);
                    }
                       
                }
                //rule of thumb for releasing com objects:  
                //  never use two dots, all COM objects must be referenced and released individually  
                //  ex: [somthing].[something].[something] is bad  
                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();

                xlWorkbook.Save();
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlWorkbook);
            }
            catch (Exception ex)
            {

               
            }
        }
        public void RefreshSheets(DataTable dt,  string strSheetName, string RawSheetName, string sourceFile,bool IsRefreshRange=true,string SourceDataType="",int TotalWeeks=5)
        {
            try
            {
                KillSpecificExcelFileProcess(sourceFile);
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbooks xlWorkbooks = xlApp.Workbooks;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlWorkbooks.Open(sourceFile);
                
                foreach (Microsoft.Office.Interop.Excel.Worksheet xlworksheet in xlWorkbook.Worksheets)
                {
                    
                        if (xlworksheet.Name == strSheetName)
                        {
                            Microsoft.Office.Interop.Excel.PivotTables pivotTbls = (Microsoft.Office.Interop.Excel.PivotTables)xlworksheet.PivotTables(Type.Missing);
                            if (pivotTbls.Count > 0)
                            {
                                for (int i = 1; i <= pivotTbls.Count; i++)
                                {
                                    if (IsRefreshRange)
                                    {
                                        if(SourceDataType=="")
                                            pivotTbls.Item(i).SourceData = RawSheetName + "!R1C1:R" + (dt.Rows.Count+1).ToString() + "C" + (dt.Columns.Count).ToString();
                                        else
                                        {
                                            if (SourceDataType=="M")
                                            {
                                                pivotTbls.Item(i).SourceData = RawSheetName + "!R18C1:R" + (dt.Rows.Count+18).ToString() + "C26";
                                            }
                                            if (SourceDataType == "W")
                                            {
                                              
                                                pivotTbls.Item(i).SourceData = RawSheetName + "!R29C1:R" + (dt.Rows.Count + 29).ToString() + "C61";
                                            //if (SourceDataType == "W" && TotalWeeks == 5)
                                            //{
                                            //    pivotTbls.Item(i).SourceData = RawSheetName + "!R15C1:R" + (dt.Rows.Count + 15).ToString() + "C53";
                                            //}
                                            //else
                                            //     pivotTbls.Item(i).SourceData = RawSheetName + "!R15C1:R" + (dt.Rows.Count+15).ToString() + "C61";
                                            }
                                    }

                                    
                                }
                                   
                                pivotTbls.Item(i).RefreshTable();
                                }
                            }

                        //////Microsoft.Office.Interop.Excel.Range xlRange = xlworksheet.UsedRange;
                        //if (SourceDataType == "W" && TotalWeeks==5)
                        //{
                        //    xlworksheet.get_Range("AW:AW").EntireColumn.Hidden = true;
                            
                        //}
                        //string finalColLetter = ExcelColumnName(dt.Columns.Count);
                        //string excelRange = string.Format("A1:{0}{1}", finalColLetter, dt.Rows.Count + 1);
                        xlworksheet.Columns.AutoFit();

                            //xlRange.Columns.AutoFit();
                            //releaseObject(xlRange);
                        }

                    

                }
                xlWorkbook.ShowPivotTableFieldList = false;
                xlWorkbook.Save();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                xlWorkbook.Close(0);

                

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlWorkbook);
                releaseObject(xlWorkbook);
                releaseObject(xlWorkbooks);
            }
            catch (Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                RefreshSheets(dt, strSheetName, RawSheetName, sourceFile,IsRefreshRange);
            }

        }
        public void RefreshSheetsTiniPass(DataTable dt, string strSheetName, string RawSheetName, string sourceFile, bool IsRefreshRange = true, string SourceDataType = "", int TotalWeeks = 5)
        {
            try
            {
                KillSpecificExcelFileProcess(sourceFile);
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbooks xlWorkbooks = xlApp.Workbooks;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlWorkbooks.Open(sourceFile);

                foreach (Microsoft.Office.Interop.Excel.Worksheet xlworksheet in xlWorkbook.Worksheets)
                {

                    if (xlworksheet.Name == strSheetName)
                    {
                        Microsoft.Office.Interop.Excel.PivotTables pivotTbls = (Microsoft.Office.Interop.Excel.PivotTables)xlworksheet.PivotTables(Type.Missing);
                        if (pivotTbls.Count > 0)
                        {
                            for (int i = 1; i <= pivotTbls.Count; i++)
                            {
                                if (IsRefreshRange)
                                {
                                    if (SourceDataType == "")
                                        pivotTbls.Item(i).SourceData = RawSheetName + "!R1C1:R" + (dt.Rows.Count + 1).ToString() + "C" + (dt.Columns.Count).ToString();
                                    else
                                    {
                                        if (SourceDataType == "M")
                                        {
                                            pivotTbls.Item(i).SourceData = RawSheetName + "!R7C1:R" + (dt.Rows.Count + 7).ToString() + "C12";
                                        }
                                        
                                    }


                                }

                                pivotTbls.Item(i).RefreshTable();
                            }
                        }

                        //////Microsoft.Office.Interop.Excel.Range xlRange = xlworksheet.UsedRange;
                        //if (SourceDataType == "W" && TotalWeeks==5)
                        //{
                        //    xlworksheet.get_Range("AW:AW").EntireColumn.Hidden = true;

                        //}
                        //string finalColLetter = ExcelColumnName(dt.Columns.Count);
                        //string excelRange = string.Format("A1:{0}{1}", finalColLetter, dt.Rows.Count + 1);
                        xlworksheet.Columns.AutoFit();

                        //xlRange.Columns.AutoFit();
                        //releaseObject(xlRange);
                    }



                }
                xlWorkbook.ShowPivotTableFieldList = false;
                xlWorkbook.Save();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                xlWorkbook.Close(0);



                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlWorkbook);
                releaseObject(xlWorkbook);
                releaseObject(xlWorkbooks);
            }
            catch (Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                RefreshSheetsTiniPass(dt, strSheetName, RawSheetName, sourceFile, IsRefreshRange);
            }

        }

        public void RefreshSheetsBillAndTraffic(DataTable dt, string strSheetName, string RawSheetName, string sourceFile, bool IsRefreshRange = true, string SourceDataType = "", int TotalWeeks = 5)
        {
            try
            {
                KillSpecificExcelFileProcess(sourceFile);
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbooks xlWorkbooks = xlApp.Workbooks;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlWorkbooks.Open(sourceFile);

                foreach (Microsoft.Office.Interop.Excel.Worksheet xlworksheet in xlWorkbook.Worksheets)
                {

                    if (xlworksheet.Name == strSheetName)
                    {
                        Microsoft.Office.Interop.Excel.PivotTables pivotTbls = (Microsoft.Office.Interop.Excel.PivotTables)xlworksheet.PivotTables(Type.Missing);
                        if (pivotTbls.Count > 0)
                        {
                            for (int i = 1; i <= pivotTbls.Count; i++)
                            {
                                if (IsRefreshRange)
                                {
                                    if (SourceDataType == "")
                                        pivotTbls.Item(i).SourceData = RawSheetName + "!R1C1:R" + (dt.Rows.Count + 1).ToString() + "C" + (dt.Columns.Count).ToString();
                                    else
                                    {
                                        if (SourceDataType == "M")
                                        {
                                            pivotTbls.Item(i).SourceData = RawSheetName + "!R7C1:R" + (dt.Rows.Count + 7).ToString() + "C12";
                                        }

                                    }


                                }

                                pivotTbls.Item(i).RefreshTable();
                            }
                        }

                        //////Microsoft.Office.Interop.Excel.Range xlRange = xlworksheet.UsedRange;
                        //if (SourceDataType == "W" && TotalWeeks==5)
                        //{
                        //    xlworksheet.get_Range("AW:AW").EntireColumn.Hidden = true;

                        //}
                        //string finalColLetter = ExcelColumnName(dt.Columns.Count);
                        //string excelRange = string.Format("A1:{0}{1}", finalColLetter, dt.Rows.Count + 1);
                        xlworksheet.Columns.AutoFit();

                        //xlRange.Columns.AutoFit();
                        //releaseObject(xlRange);
                    }



                }
                xlWorkbook.ShowPivotTableFieldList = false;
                xlWorkbook.Save();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                xlWorkbook.Close(0);



                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlWorkbook);
                releaseObject(xlWorkbook);
                releaseObject(xlWorkbooks);
            }
            catch (Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                RefreshSheetsBillAndTraffic(dt, strSheetName, RawSheetName, sourceFile, IsRefreshRange);
            }

        }

        public void RefreshSheetsTopUpCampaign(DataTable dt, string strSheetName, string RawSheetName, string sourceFile, bool IsRefreshRange = true, string SourceDataType = "", int TotalWeeks = 5)
        {
            try
            {
                KillSpecificExcelFileProcess(sourceFile);
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbooks xlWorkbooks = xlApp.Workbooks;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlWorkbooks.Open(sourceFile);

                foreach (Microsoft.Office.Interop.Excel.Worksheet xlworksheet in xlWorkbook.Worksheets)
                {

                    if (xlworksheet.Name == strSheetName)
                    {
                        Microsoft.Office.Interop.Excel.PivotTables pivotTbls = (Microsoft.Office.Interop.Excel.PivotTables)xlworksheet.PivotTables(Type.Missing);
                        if (pivotTbls.Count > 0)
                        {
                            for (int i = 1; i <= pivotTbls.Count; i++)
                            {
                                if (IsRefreshRange)
                                {
                                    if (SourceDataType == "")
                                        pivotTbls.Item(i).SourceData = RawSheetName + "!R1C1:R" + (dt.Rows.Count + 1).ToString() + "C" + (dt.Columns.Count).ToString();
                                    else
                                    {
                                        if (SourceDataType == "M")
                                        {
                                            pivotTbls.Item(i).SourceData = RawSheetName + "!R7C1:R" + (dt.Rows.Count + 7).ToString() + "C12";
                                        }

                                    }


                                }

                                pivotTbls.Item(i).RefreshTable();
                            }
                        }

                        //////Microsoft.Office.Interop.Excel.Range xlRange = xlworksheet.UsedRange;
                        //if (SourceDataType == "W" && TotalWeeks==5)
                        //{
                        //    xlworksheet.get_Range("AW:AW").EntireColumn.Hidden = true;

                        //}
                        //string finalColLetter = ExcelColumnName(dt.Columns.Count);
                        //string excelRange = string.Format("A1:{0}{1}", finalColLetter, dt.Rows.Count + 1);
                        xlworksheet.Columns.AutoFit();

                        //xlRange.Columns.AutoFit();
                        //releaseObject(xlRange);
                    }



                }
                xlWorkbook.ShowPivotTableFieldList = false;
                xlWorkbook.Save();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                xlWorkbook.Close(0);



                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlWorkbook);
                releaseObject(xlWorkbook);
                releaseObject(xlWorkbooks);
            }
            catch (Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                RefreshSheetsTopUpCampaign(dt, strSheetName, RawSheetName, sourceFile, IsRefreshRange);
            }

        }


        public void RefreshRangeSheets(string sourceFile, List<RefreshData> lsRefresh)
        {
            try
            {
                KillSpecificExcelFileProcess(sourceFile);
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbooks xlWorkbooks = xlApp.Workbooks;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlWorkbooks.Open(sourceFile);
                foreach(RefreshData rf in lsRefresh)
                {
                    foreach (Microsoft.Office.Interop.Excel.Worksheet xlworksheet in xlWorkbook.Worksheets)
                    {

                        if (xlworksheet.Name == rf.SheetName)
                        {
                            Microsoft.Office.Interop.Excel.PivotTables pivotTbls = (Microsoft.Office.Interop.Excel.PivotTables)xlworksheet.PivotTables(Type.Missing);
                            if (pivotTbls.Count > 0)
                            {
                                for (int i = 1; i <= pivotTbls.Count; i++)
                                {
                                    if(rf.StartRow>1 && rf.StartColumn>1)
                                        pivotTbls.Item(i).SourceData = rf.SheetName + "!R"+rf.StartRow.ToString()+ "C"+rf.StartColumn.ToString()+":R" + (rf.SourceData.Rows.Count + rf.StartRow).ToString() + "C" + (rf.SourceData.Columns.Count+rf.StartColumn).ToString();
                                    else
                                        pivotTbls.Item(i).SourceData = rf.SheetName + "!R1C1:R" + (rf.SourceData.Rows.Count + 1).ToString() + "C" + (rf.SourceData.Columns.Count).ToString();
                                    pivotTbls.Item(i).RefreshTable();
                                }
                            }

                            xlworksheet.Columns.AutoFit();

                        }



                    }
                }    
                
                xlWorkbook.ShowPivotTableFieldList = false;
                xlWorkbook.Save();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                xlWorkbook.Close(0);



                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);
                releaseObject(xlWorkbook);
                releaseObject(xlWorkbook);
                releaseObject(xlWorkbooks);
            }
            catch (Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                RefreshRangeSheets(sourceFile,lsRefresh);
            }

        }



        public void WriteRangeData(string TemplateFileName, List<WriteData> lsWriteData)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(TemplateFileName); //strDesFullDirect
                foreach (WriteData wdt in lsWriteData)
                {
                    if (xlWorkbook.Sheets[wdt.SheetName] != null)
                    {
                        Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[wdt.SheetName];
                        Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;
                        object misValue = System.Reflection.Missing.Value;

                        int col, row;
                        object[,] rawData = new object[wdt.Datasource.Rows.Count + wdt.StartRow, wdt.Datasource.Columns.Count - 1 + +wdt.StartColumn];

                        for (col = 0; col <= wdt.Datasource.Columns.Count - 1; col++)
                            rawData[0, col] = wdt.Datasource.Columns[col].ColumnName;

                        for (col = 0; col <= wdt.Datasource.Columns.Count - 1; col++)
                        {
                            for (row = 0; row <= wdt.Datasource.Rows.Count - 1; row++)
                                rawData[row + 1, col] = wdt.Datasource.Rows[row].ItemArray[col];
                        }
                        string finalColLetter = ExcelColumnName(wdt.Datasource.Columns.Count);
                        string excelRange = string.Format("A"+ wdt.StartRow.ToString() + ":{0}{1}", finalColLetter, wdt.Datasource.Rows.Count + wdt.StartRow);
                        xlWorksheet.Range[excelRange, Type.Missing].Value2 = rawData;
                        xlWorksheet.Columns.AutoFit();
                        //for (int i = 0; i < dataTable.Rows.Count; i++)
                        //{
                        //    for (int j = 0; j < dataTable.Columns.Count; j++)
                        //    {
                        //        var obj = dataTable.Rows[i][j];
                        //        xlWorksheet.Cells[i + startRow, startCol + j] = obj.ToString();
                        //    }
                        //}


                        //cleanup  
                        GC.Collect();
                        GC.WaitForPendingFinalizers();






                        if (wdt.IsSaveFile == true)
                        {
                            xlWorkbook.RefreshAll();

                            xlApp.Application.CalculateFull();

                            xlWorksheet.Cells[wdt.StartRow, wdt.Datasource.Columns.Count + 1] = "Last update time: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                            xlWorkbook.SaveAs(wdt.SaveFileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                        }
                        else
                            xlWorkbook.Save();
                        releaseObject(xlRange);
                        releaseObject(xlWorksheet);
                    }

                }
                //close and release  
                xlWorkbook.Close();

                //quit and release  
                xlApp.Quit();

                //release com objects to fully kill excel process from running in the background  
                releaseObject(xlApp);

                releaseObject(xlWorkbook);
            }
            catch (Exception ex)
            {
                Thread.Sleep(100000);
                KillSpecificExcelFileProcess("");
                WriteRangeData(TemplateFileName, lsWriteData);
            }
        }


    }
}
