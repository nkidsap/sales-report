﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Dapper;
using MoralesLarios.Data.Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SalesReport.Class;
using SalesReport.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalesReport
{
    public partial class Form1 : Form
    {

        private const string bucketName = @"nkid-report/Sales_Report/Retail";// "*** provide bucket name ***";
        private const string keyName = "AKIAJAFK5TMDIEW4NMJA";// " * ** provide a name for the uploaded object ***";

        private const string secretKey = "eyFT6oMlItl5rJQ7XZ42yZWODQCmhPVbAr8ymKu9";
        //private const string filePath = "*** provide the full path name of the file to upload ***";
        // Specify your bucket region (an example region is shown).
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.APSoutheast1;
        private static IAmazonS3 s3Client;
        string filepathAWS = "";

        static ExcelUtilities excelUtilities = new ExcelUtilities();
        static int fromHours = 5;
        static int toHours = 20;
        static int _interval = 3600000;

        public Form1()
        {
            InitializeComponent();
        }
        private static void UploadFile(string filePath)
        {
            try
            {
                var fileTransferUtility =   new Amazon.S3.Transfer.TransferUtility(s3Client);

                // Option 1. Upload a file. The file name is used as the object key name.
                fileTransferUtility.Upload(filePath, bucketName);
                Console.WriteLine("Upload 1 completed");

                //// Option 2. Specify object key name explicitly.
                //fileTransferUtility.Upload(filePath, bucketName, keyName);
                //Console.WriteLine("Upload 2 completed");

                //// Option 3. Upload data from a type of System.IO.Stream.
                //using (var fileToUpload =
                //    new FileStream(filePath, FileMode.Open, FileAccess.Read))
                //{
                //    fileTransferUtility.Upload(fileToUpload,
                //                               bucketName, keyName);
                //}
                //Console.WriteLine("Upload 3 completed");

                //// Option 4. Specify advanced settings.
                //var fileTransferUtilityRequest = new Amazon.S3.Transfer.TransferUtilityUploadRequest
                //{
                //    BucketName = bucketName,
                //    FilePath = filePath,
                //    StorageClass = S3StorageClass.StandardInfrequentAccess,
                //    PartSize = 6291456, // 6 MB.
                //    Key = keyName,
                //    CannedACL = S3CannedACL.PublicRead
                //};
                //fileTransferUtilityRequest.Metadata.Add("param1", "Value1");
                //fileTransferUtilityRequest.Metadata.Add("param2", "Value2");

                //fileTransferUtility.Upload(fileTransferUtilityRequest);
                //Console.WriteLine("Upload 4 completed");
            }

            catch (AmazonS3Exception e)
            {
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + " - AmazonS3Exception: " + e.Message.ToString());
                Thread.Sleep(100000);
                UploadFile(filePath);
                //Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() +" - "+ e.Message.ToString());
                Thread.Sleep(100000);
                UploadFile(filePath);
                //Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }

        }
        private static void UploadFileNkidCloud(string filePath)
        {
            try
            {
                //Create FTP request
                FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create("ftp://cloud.nkidcorp.com/REPORT/Sales_Report/TopUp_Campaign/" + Path.GetFileName(filePath));

                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential("backup_it", "4Q:j[jqs_U9d");
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                //Load the file
                FileStream stream = File.OpenRead(filePath);
                byte[] buffer = new byte[stream.Length];

                stream.Read(buffer, 0, buffer.Length);
                stream.Close();

                //Upload file
                Stream reqStream = request.GetRequestStream();
                reqStream.Write(buffer, 0, buffer.Length);
                reqStream.Close();

                request = null;
                WriteLog("Uploaded Successfully");


               
            }

            catch (AmazonS3Exception e)
            {
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + " - UploadFTPException: " + e.Message.ToString());
                Thread.Sleep(100000);
                UploadFileNkidCloud(filePath);
                //Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + " - " + e.Message.ToString());
                Thread.Sleep(100000);
                UploadFileNkidCloud(filePath);
                //Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }

        }

        private void btSendMail_Click(object sender, EventArgs e)
        {
            RerunJobAllTiNiPass_Transactions();
            SendMailMarketingOnlineSaleReport();
            SendMailTrendingRevenueReport();
            SendMailSalesReport();
            SendMailTiniPassSalesReport();
            SendMailBillAndTrafficReport();
            SendMailVolumeAnalyticsReport();
            SendMailTopUpCampaignReport();

        }
       
        private static void SendMailSalesReport()
        {
            string message = string.Empty;
            try
            {
                

                    s3Client = new AmazonS3Client(keyName, secretKey, bucketRegion);

                    string strConfigFromDate = ConfigurationManager.AppSettings["strFromDate"];
                    string strConfigToDate = ConfigurationManager.AppSettings["strToDate"];


                    string rev_temp = AppDomain.CurrentDomain.BaseDirectory + @"\Temp\Retail_Revenue_Report.xlsx";
                    string rev_source = AppDomain.CurrentDomain.BaseDirectory + @"\DataSource\Retail_Revenue_Report.xlsx";

                    rev_temp = rev_temp.Replace("\\\\", "\\");
                    rev_source = rev_source.Replace("\\\\", "\\");

                    
                    var curDate = DateTime.Today.AddDays(-1);
                    DateTime dFromDate = new DateTime(curDate.Year, curDate.Month, 1);
                    DateTime dToDate = DateTime.Today.AddDays(-1);

                    if (!string.IsNullOrWhiteSpace(strConfigFromDate))
                        dFromDate = Convert.ToDateTime(strConfigFromDate);

                    if (!string.IsNullOrWhiteSpace(strConfigToDate))
                        dToDate = Convert.ToDateTime(strConfigToDate);

                    Console.WriteLine("Initializing SalesReport...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Initializing SalesReport...");
                    SetExcelUtility(dFromDate, dToDate);

                    excelUtilities.KillSpecificExcelFileProcessBackgroud();

                    excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                    if (File.Exists(rev_temp))
                        File.Delete(rev_temp);
                    excelUtilities.KillSpecificExcelFileProcess(rev_source);
                    if (File.Exists(rev_source))
                        File.Delete(rev_source);
                    excelUtilities.KillSpecificExcelFileProcess("");





                    Console.WriteLine("Getting Data SalesReport...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Getting Data SalesReport...");
                    var dtResults = GetDataDailySalesReport();
                    var dtResults_LastMonth = GetDataDailySalesReportLastMonth();
                    var dtResults_LastYear = GetDataDailySalesReportLastYear();
                    var dtStoreOpen = GetDataStoreOpenList();


                    Console.WriteLine("Removing Sheet Raw Data SalesReport...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Removing Sheet Raw Data SalesReport...");
                    excelUtilities.RemoveRange(new List<string>() {  "RAW" ,"RAW_LM", "RAW_LY","STORES" });

                    Console.WriteLine("Insert Data SalesReport...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Insert Data SalesReport...");
                    //excelUtilities.WriteData(dtResults, sheetArr, excelUtilities.strDesFullDirect, 1, 2, dToDate);
                    excelUtilities.KillSpecificExcelFileProcess(rev_temp);

                    excelUtilities.WriteData1(dtStoreOpen.Tables[0], "STORES", 1, 1, rev_temp, false);
                    excelUtilities.WriteData1(dtResults_LastMonth.Tables[0], "RAW_LM", 1, 1, rev_temp, false);
                    excelUtilities.WriteData1(dtResults_LastYear.Tables[0], "RAW_LY", 1, 1, rev_temp, false);
                    excelUtilities.WriteData1(dtResults.Tables[0], "RAW", 1, 1, rev_temp, true);

                    excelUtilities.HideSheet(rev_temp, "REGION");
                    excelUtilities.HideSheet(rev_temp, "CLASSIFICATION");

                    excelUtilities.HideSheet(rev_temp, "RAWWEEK_DATA");
                    excelUtilities.HideSheet(rev_temp, "RAWMONTH_DATA");
                    excelUtilities.HideSheet(rev_temp, "FY 20' BUDGET");

                    Console.WriteLine("Refresh Pivot Data SalesReport...");
                    
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Refresh Pivot Data SalesReport...");
                    excelUtilities.RefreshSheets(dtResults.Tables[0], "DAILY SALE REPORT", "RAW" , rev_temp);

                    excelUtilities.RefreshSheets(dtStoreOpen.Tables[0], "MONTHLY SALE REPORT", "RAWMONTH_DATA", rev_temp,true,"M");
                    excelUtilities.RefreshSheets(dtStoreOpen.Tables[0], "WEEKLY SALE REPORT", "RAWWEEK_DATA", rev_temp, true, "W",GetTotalWeekOfMonth());




                    excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                    excelUtilities.KillSpecificExcelFileProcess("");
                    File.Copy(rev_temp, rev_source);
                    //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Begin Upload file ...");
                    //UploadFile( rev_source);
                    //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-End Upload file...");
                    Console.WriteLine("Send Mail...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail SalesReport...");
                    SendEmailByBU("RET", new List<string>() { "c293dede.nkidgroup.com@apac.teams.ms" }, AppDomain.CurrentDomain.BaseDirectory + @"\Body.html", "http://gofile.me/6zCdF/lNmEyyBPZ", rev_source);
                   

                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Done SalesReport...");
                    WriteLog("-------###-------");


            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                message = ex.Message;
            }

            if (!string.IsNullOrWhiteSpace(message))
                SendErrorMessage(message);

            //Console.ReadLine();
        
        }
        private static void SendMailTiniPassSalesReport()
        {
            string message = string.Empty;
            try
            {
               
                    s3Client = new AmazonS3Client(keyName, secretKey, bucketRegion);

                    string strConfigFromDate = ConfigurationManager.AppSettings["strFromDate"];
                    string strConfigToDate = ConfigurationManager.AppSettings["strToDate"];

                    string rev_template = AppDomain.CurrentDomain.BaseDirectory + @"\TiniPass_Revenue_Report.xlsx";

                    string rev_temp = AppDomain.CurrentDomain.BaseDirectory + @"\Temp\TiniPass_Revenue_Report.xlsx";
                    string rev_source = AppDomain.CurrentDomain.BaseDirectory + @"\DataSource\TiniPass_Revenue_Report.xlsx";

                    rev_template = rev_template.Replace("\\\\", "\\");
                    rev_temp = rev_temp.Replace("\\\\", "\\");
                    rev_source = rev_source.Replace("\\\\", "\\");


                    var curDate = DateTime.Today.AddDays(-1);
                    DateTime dFromDate = new DateTime(curDate.Year, curDate.Month, 1);
                    DateTime dToDate = DateTime.Today.AddDays(-1);

                    if (!string.IsNullOrWhiteSpace(strConfigFromDate))
                        dFromDate = Convert.ToDateTime(strConfigFromDate);

                    if (!string.IsNullOrWhiteSpace(strConfigToDate))
                        dToDate = Convert.ToDateTime(strConfigToDate);

                    Console.WriteLine("Initializing TiniPass...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Initializing TiniPass...");
                    SetExcelUtility(dFromDate, dToDate);

                    excelUtilities.KillSpecificExcelFileProcessBackgroud();

                    excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                    if (File.Exists(rev_temp))
                        File.Delete(rev_temp);
                    excelUtilities.KillSpecificExcelFileProcess(rev_source);
                    if (File.Exists(rev_source))
                        File.Delete(rev_source);
                    excelUtilities.KillSpecificExcelFileProcess("");




                    dFromDate = new DateTime(curDate.Year, 5, 28);
                    dToDate = new DateTime(curDate.Year, 12, 31);
                    Console.WriteLine("Getting Data TiniPass...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Getting Data TiniPass...");
                    var dtResults = GetDataDailyTiniPassSalesReport();
                    var dtResults_All = GetDataDailyTiniPassAllSalesReport();
                    var dtStoreOpen = GetDataStoreTiniPassList();
                    var dtWeekly = GetTiniPassWeekly(dFromDate, dToDate);
                    var dtTopUpEcode = GetTopUpEcode();



                   Console.WriteLine("Removing Sheet Raw Data TiniPass...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Removing Sheet Raw Data TiniPass...");
                    //excelUtilities.RemoveRange(new List<string>() { "RAW_DATA", "ALL_RAW_DATA", "STORES" }, rev_template);
                    excelUtilities.RemoveRange(new List<string>() { "RAW_DATA", "ALL_RAW_DATA", "STORES", "tiNiPass_Weekly_Raw_Data", "TopUpEcode" }, rev_template);

                    Console.WriteLine("Insert Data TiniPass...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Insert Data TiniPass...");
                    //excelUtilities.WriteData(dtResults, sheetArr, excelUtilities.strDesFullDirect, 1, 2, dToDate);

                    excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                    excelUtilities.WriteDataTiniPass(dtStoreOpen.Tables[0], "STORES", 1, 1, rev_temp, false);
                    excelUtilities.WriteDataTiniPass(dtResults_All.Tables[0], "ALL_RAW_DATA", 1, 1, rev_temp, false);
                    excelUtilities.WriteDataTiniPass(dtWeekly.Tables[0], "tiNiPass_Weekly_Raw_Data", 1, 1, rev_temp, false);
                    excelUtilities.WriteDataTiniPass(dtTopUpEcode.Tables[0], "TopUpEcode", 1, 1, rev_temp, false);
                    excelUtilities.WriteDataTiniPass(dtResults.Tables[0], "RAW_DATA", 1, 1, rev_temp, true);

                    excelUtilities.HideSheet(rev_temp, "RAW_DATA");
                    excelUtilities.HideSheet(rev_temp, "ALL_RAW_DATA");
                    excelUtilities.HideSheet(rev_temp, "STORES");
                    excelUtilities.HideSheet(rev_temp, "tiNiPass_Weekly_Raw_Data");
                    excelUtilities.HideSheet(rev_temp, "TopUpEcode");

                    excelUtilities.HideSheet(rev_temp, "REGION");
                    excelUtilities.HideSheet(rev_temp, "CLASSIFICATION");
                    excelUtilities.HideSheet(rev_temp, "MTD_ROWDATA");
                    excelUtilities.HideSheet(rev_temp, "REV_BUDGET");
                    excelUtilities.HideSheet(rev_temp, "VOL_BUDGET");

                    Console.WriteLine("Refresh Pivot Data TiniPass...");

                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Refresh Pivot Data TiniPass...");
                    excelUtilities.RefreshSheetsTiniPass(dtStoreOpen.Tables[0], "MTD", "MTD_ROWDATA", rev_temp,true,"M");
                    excelUtilities.RefreshSheetsTiniPass(dtResults.Tables[0], "tiNiPass", "RAW_DATA", rev_temp, true);
                    
                    excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                    excelUtilities.KillSpecificExcelFileProcess("");
                    File.Copy(rev_temp, rev_source);
                    //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Begin Upload file ...");
                    //UploadFile(rev_source);
                    //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-End Upload file...");
                    Console.WriteLine("Send Mail TiniPass...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail TiniPass...");
                    SendEmailTiniPass( new List<string>() { "678832e2.nkidgroup.com@apac.teams.ms" }, AppDomain.CurrentDomain.BaseDirectory + @"\Body_TiniPass.html", rev_source);


                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Done TiniPass...");
                    WriteLog("-------###-------");


            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                message = ex.Message;
            }

            if (!string.IsNullOrWhiteSpace(message))
                SendErrorMessage(message);

            //Console.ReadLine();

        }

        private static void SendMailBillAndTrafficReport()
        {
            string message = string.Empty;
            try
            {
               


                    s3Client = new AmazonS3Client(keyName, secretKey, bucketRegion);

                    string strConfigFromDate = ConfigurationManager.AppSettings["strFromDate"];
                    string strConfigToDate = ConfigurationManager.AppSettings["strToDate"];

                    string rev_template = AppDomain.CurrentDomain.BaseDirectory + @"\Traffic_And_Bill_Report.xlsx";

                    string rev_temp = AppDomain.CurrentDomain.BaseDirectory + @"\Temp\Traffic_And_Bill_Report.xlsx";
                    string rev_source = AppDomain.CurrentDomain.BaseDirectory + @"\DataSource\Traffic_And_Bill_Report.xlsx";

                    rev_template = rev_template.Replace("\\\\", "\\");
                    rev_temp = rev_temp.Replace("\\\\", "\\");
                    rev_source = rev_source.Replace("\\\\", "\\");


                    var curDate = DateTime.Today.AddDays(-1);
                    DateTime dFromDate = new DateTime(curDate.Year, curDate.Month, 1);
                    DateTime dToDate = DateTime.Today.AddDays(-1);

                    if (!string.IsNullOrWhiteSpace(strConfigFromDate))
                        dFromDate = Convert.ToDateTime(strConfigFromDate);

                    if (!string.IsNullOrWhiteSpace(strConfigToDate))
                        dToDate = Convert.ToDateTime(strConfigToDate);

                    Console.WriteLine("Initializing BillAndTraffic...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Initializing BillAndTraffic...");
                    SetExcelUtility(dFromDate, dToDate);

                    excelUtilities.KillSpecificExcelFileProcessBackgroud();

                    excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                    if (File.Exists(rev_temp))
                        File.Delete(rev_temp);
                    excelUtilities.KillSpecificExcelFileProcess(rev_source);
                    if (File.Exists(rev_source))
                        File.Delete(rev_source);
                    excelUtilities.KillSpecificExcelFileProcess("");





                    Console.WriteLine("Getting Data BillAndTraffic...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Getting Data BillAndTraffic...");
                    var dtResults = GetDataBillAndTrafficReport();
                   


                    Console.WriteLine("Removing Sheet Raw Data BillAndTraffic...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Removing Sheet Raw Data BillAndTraffic...");
                    excelUtilities.RemoveRange(new List<string>() { "RAW_DATA" }, rev_template);

                    Console.WriteLine("Insert Data BillAndTraffic...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Insert Data BillAndTraffic...");
                    //excelUtilities.WriteData(dtResults, sheetArr, excelUtilities.strDesFullDirect, 1, 2, dToDate);

                    excelUtilities.KillSpecificExcelFileProcess(rev_temp);

                    excelUtilities.WriteDataBillAndTraffic(dtResults.Tables[0], "RAW_DATA", 1, 1, rev_temp, true);

                    excelUtilities.HideSheet(rev_temp, "RAW_DATA");
                

                    Console.WriteLine("Refresh Pivot Data BillAndTraffic...");

                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Refresh Pivot Data BillAndTraffic...");
                    excelUtilities.RefreshSheetsBillAndTraffic(dtResults.Tables[0], "RET", "RAW_DATA", rev_temp, true);
                    excelUtilities.RefreshSheetsBillAndTraffic(dtResults.Tables[0], "ATT", "RAW_DATA", rev_temp, true);

                    excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                    excelUtilities.KillSpecificExcelFileProcess("");
                    File.Copy(rev_temp, rev_source);
                    //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Begin Upload file ...");
                    //UploadFile(rev_source);
                    //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-End Upload file...");
                    Console.WriteLine("Send Mail BillAndTraffic...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail BillAndTraffic...");
                    SendEmailBillAndTraffic(new List<string>() { "b76dcb0b.nkidgroup.com@apac.teams.ms","Novia.Ng@affirmacapital.com","Thuy.Dropsey@affirmacapital.com" }, AppDomain.CurrentDomain.BaseDirectory + @"\Body_Bill_Traffic.html", rev_source);


                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Done BillAndTraffic...");
                    WriteLog("-------###-------");



               
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                message = ex.Message;
            }

            if (!string.IsNullOrWhiteSpace(message))
                SendErrorMessage(message);

            //Console.ReadLine();

        }

        private static void ExportCSV(DataTable dt,string filepath)
        {
            StringBuilder sb = new StringBuilder();
            IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().Select(column => column.ColumnName);

            sb.AppendLine(string.Join(",", columnNames));
            foreach (DataRow row in dt.Rows)
            {
                var a = row.ItemArray;
                IEnumerable<string> fields = row.ItemArray.Select(field =>
                (field.ToString() == "" ? "NULL" : field.ToString()).Replace(",", " "));
                sb.AppendLine(string.Join(",", fields));
            }

            try
            {
                // ghi log
                //var dir = @"D:\EXPORT EXCEL\DATA\KT0000";
                //string filename = string.Format("KT2020_Topup_Bill_{0}_{1}.csv", (month < 10 ? "0" : "") + month.ToString(), year.ToString());
                //string filename = string.Format("KT0000_Store_List.csv");

                
                File.WriteAllText(filepath, sb.ToString());

            }
            catch (Exception ex)
            {
                
            }
        }
        private static void WriteLog(string Message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Log.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + Message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        private static void SetConnection()
        {
            string strConnectionString = ConfigurationManager.AppSettings["strConnectionString"];
            string enc = Encrypt.fDecrypt(strConnectionString);

            cFunctions.conStr = enc;
            cFunctions functions = new cFunctions();
        }
        private static DataTable  GetTimeStart()
        {
            SetConnection();


            var dtResults = cFunctions.Instance.Select("select * from vw_TimerReport");

            return dtResults;
        }
       
        private static DataSet GetDataDailySalesReport()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("USP_DAILY_SALE_REPORT", storeParams);

            return dtResults;
        }
        private static DataSet GetDataDailyTiniPassSalesReport()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("USP_DAILY_SALE_REPORT_TINI_PASS", storeParams);

            return dtResults;
        }

        private static DataSet GetDataReport(string storename, Hashtable storeParams)
        {
            SetConnection();

            //Hashtable storeParams = new Hashtable();
            ////storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            ////storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            //////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            ////storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc(storename, storeParams);

            return dtResults;
        }

        private static DataSet GetDataFreeTicketVolumeReport()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("USP_VOLUME_FREE_TICKET", storeParams);

            return dtResults;
        }
        private static DataSet GetDataDiscountTicketVolumeReport()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("USP_VOLUME_DISCOUNT_TICKET", storeParams);

            return dtResults;
        }

        private static DataSet GetDataInvitationTicketReport()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("USP_InvitationTicketInCenter", storeParams);

            return dtResults;
        }

        private static DataSet GetDataCardLevelVolumeReport()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("USP_VOLUME_ANALYTICS_REPORT_TINI_PASS_CARDLEVEL_VOLUME", storeParams);

            return dtResults;
        }

        private static DataSet GetDataBillAndTrafficReport()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("USP_DAILY_TRAFFIC_AND_BILLS_REPORT", storeParams);

            return dtResults;
        }
        private static DataSet GetDataDailyTiniPassAllSalesReport()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("USP_DAILY_SALE_REPORT_TINI_PASS_ALL_VOLUME", storeParams);

            return dtResults;
        }
        private static DataSet GetDataDailySalesReportLastMonth()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("USP_DAILY_SALE_REPORT_PREV_MONTH", storeParams);

            return dtResults;
        }
        private static DataSet GetDataDailySalesReportLastYear()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("USP_DAILY_SALE_REPORT_PREV_YEAR", storeParams);

            return dtResults;
        }
        private static bool RerunJobAllTiNiPass_Transactions()
        {
            try
            {
                SetConnection();

                Hashtable storeParams = new Hashtable();
                //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
                //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
                ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
                //storeParams.Add("BU", BU);
                cFunctions.Instance.ExcuteProc("USP_JOB_AllTiNiPass_Transactions", storeParams);

                return true;
            }
            catch (Exception)
            {

                return false;
            }
            
        }

        private static DataSet GetDataGet_TopUp_Game(DateTime dFromDate, DateTime dToDate)
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("[ANALYTICS_DATA].dbo.USP_GET_TOPUP_GAME_TRANSACTION", storeParams);

            return dtResults;
        }

        private static DataSet GetDataGet_Spending_By_CardNo(DateTime dFromDate, DateTime dToDate)
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("[ANALYTICS_DATA].dbo.USP_GET_SPENDING_BY_CARDNO_TRANSACTION", storeParams);

            return dtResults;
        }

        private static DataSet GetDataGet_RawSpending(DateTime dFromDate, DateTime dToDate)
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("[115.75.6.184].[ANALYTICS_POS_DATA].dbo.USP_GET_RAW_SPENDING_TRANSACTIONS", storeParams);

            return dtResults;
        }

        private static DataSet GetDataGet_Membership(DateTime dFromDate, DateTime dToDate)
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("[ANALYTICS_DATA].dbo.USP_GET_SPENDING_BY_MEMBERSHIP_TINIPASS_TRANSACTION", storeParams);

            return dtResults;
        }

        private static DataSet GetDataStoreOpenList()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("USP_LIST_STORE_OPEN", storeParams);

            return dtResults;
        }

        private static DataSet GetDataStoreTiniPassList()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            var dtResults = cFunctions.Instance.ExcuteProc("USP_LIST_STORE_TINI_PASS", storeParams);

            return dtResults;
        }

        private static DataSet GetTiniPassWeekly(DateTime From_Date, DateTime To_Date)
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            storeParams.Add("Data_Type", "");
            storeParams.Add("Store_Code", "");
            storeParams.Add("View_Type", "DETAILS");
            storeParams.Add("From_Date", From_Date.ToString("yyyyMMdd"));
            storeParams.Add("To_Date", To_Date.ToString("yyyyMMdd"));

            var dtResults = cFunctions.Instance.ExcuteProc("[115.75.6.184].[ANALYTICS_POS_DATA].[dbo].[USP_GET_SALES_RP_ONEPOS]", storeParams);

            return dtResults;
        }

        private static DataSet GetData_Trending_Revenue_Daily()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("Data_Type", "");
            //storeParams.Add("Store_Code", "");
            //storeParams.Add("View_Type", "DETAILS");
            //storeParams.Add("From_Date", From_Date.ToString("yyyyMMdd"));
            //storeParams.Add("To_Date", To_Date.ToString("yyyyMMdd"));

            var dtResults = cFunctions.Instance.ExcuteProc("[115.75.6.184].[ANALYTICS_POS_DATA].[dbo].[USP_ATT_Trending_Revenue_Daily]", storeParams);

            return dtResults;
        }


        private static DataSet GetTopUpEcode()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("Data_Type", "");
            //storeParams.Add("Store_Code", "");
            //storeParams.Add("View_Type", "DETAILS");
            //storeParams.Add("From_Date", From_Date.ToString("yyyyMMdd"));
            //storeParams.Add("To_Date", To_Date.ToString("yyyyMMdd"));

            var dtResults = cFunctions.Instance.ExcuteProc("[115.75.6.184].[ANALYTICS_POS_DATA].[dbo].[USP_POS_TopUpEcode]", storeParams);

            return dtResults;
        }


        private static int GetTotalWeekOfMonth()
        {
            SetConnection();

            Hashtable storeParams = new Hashtable();
            //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
            //storeParams.Add("ToDate", dToDate.ToString("yyyyMMdd"));
            ////var dtResults = cFunctions.Instance.ExcuteProc("CASHFLOW_RP", storeParams);
            //storeParams.Add("BU", BU);
            return cFunctions.Instance.GetValue("USP_GET_TOTAL_WEEK_OF_MONTH", storeParams);

        }

        private static void SetExcelUtility(DateTime dFrmDate, DateTime dToDate)
        {
            string strConfigSourcePath = ConfigurationManager.AppSettings["strSourcePath"];
            string strConfigSourceName = ConfigurationManager.AppSettings["strSourceFileName"];
            string strConfigDesPath = ConfigurationManager.AppSettings["strDesPath"];
            string strConfigDesFileName = ConfigurationManager.AppSettings["strDesFileName"];
            string strConfigDesDisplaySheet = ConfigurationManager.AppSettings["strDesDisplaySheet"];

            if (string.IsNullOrWhiteSpace(strConfigSourcePath))
                throw new Exception("Not found Source FilePath");
            if (string.IsNullOrWhiteSpace(strConfigSourceName))
                throw new Exception("Not found Source FileName");
            if (string.IsNullOrWhiteSpace(strConfigDesPath))
                throw new Exception("Not found Source strDesPath");
            if (string.IsNullOrWhiteSpace(strConfigDesFileName))
                throw new Exception("Not found Source strDesFileName");


            excelUtilities.strSourcePath = strConfigSourcePath.ToString();
            excelUtilities.strSourceFileName = strConfigSourceName.ToString();
            excelUtilities.strDesPath = strConfigDesPath.ToString();
            excelUtilities.strDesFileName = string.Format(strConfigDesFileName, dToDate.ToString("dd.MM.yyyy"));
        }
       
        private static void SendEmail()
        {
            bool result = false;
            try
            {

                List<string> strListOfReceivers = ConfigurationManager.AppSettings["strListOfReceivers"].Split('|').ToList();
                List<string> strListofCc = ConfigurationManager.AppSettings["strListOfCC"].Split('|').ToList();
                string strConfigSender = ConfigurationManager.AppSettings["strSender"];
                string strConfigPassword = ConfigurationManager.AppSettings["strPassword"];
                string strPassword = Encrypt.fDecrypt(strConfigPassword);

                SmtpClient client = new SmtpClient("smtp.office365.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential(strConfigSender, strPassword);
                MailAddress from = new MailAddress(strConfigSender, String.Empty, System.Text.Encoding.UTF8);
                MailAddress to = new MailAddress(strListOfReceivers[0]);
                MailMessage message = new MailMessage(from, to);


                for (int i = 0; i < strListOfReceivers.Count; i++)
                {
                    if (i == 0)
                        continue;

                    MailAddress receipent = new MailAddress(strListOfReceivers[i]);
                    message.To.Add(receipent);
                }

                //for (int i = 0; i < strListofCc.Count; i++)
                //{
                //    MailAddress receipent = new MailAddress(strListofCc[i]);
                //    message.CC.Add(receipent);
                //}


                string strBodyPath = ConfigurationManager.AppSettings["strBodyPath"];
                var bodyMail = File.ReadAllText(strBodyPath).Replace("\r\n", string.Empty);
                bodyMail = bodyMail.Replace("@UpdateTime", DateTime.Now.ToString("HH:mm:ss"));
                message.Body = bodyMail;
                message.IsBodyHtml = true;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.Subject = "Inventory Daily Reporting -" + DateTime.Now.ToString("dd/MM/yyyy");
                message.SubjectEncoding = System.Text.Encoding.UTF8;

                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectATT));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectRET));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectDIS));
               
                client.Send(message);
                
                message.Dispose();
                Console.WriteLine("Sent email successfully");
                result = true;
            }
            catch 
            {
                Thread.Sleep(300000);
                SendEmail();
            }
            if (!result)
            {
                Thread.Sleep(300000);
                SendEmail();
            }
        }
        private static void SendEmailByBU(string BU, List<string> Receivers, string BodyPath,string URL, string FileAttachment)
        {
            bool result = false;
            try
            {

                List<string> strListOfReceivers = Receivers;
                List<string> strListofCc = ConfigurationManager.AppSettings["strListOfCC"].Split('|').ToList();
                string strConfigSender = ConfigurationManager.AppSettings["strSender"];
                string strConfigPassword = ConfigurationManager.AppSettings["strPassword"];
                string strPassword = Encrypt.fDecrypt(strConfigPassword);

                SmtpClient client = new SmtpClient("smtp.office365.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential(strConfigSender, strPassword);
                MailAddress from = new MailAddress(strConfigSender, String.Empty, System.Text.Encoding.UTF8);
                MailAddress to = new MailAddress(strListOfReceivers[0]);
                MailMessage message = new MailMessage(from, to);


                for (int i = 0; i < strListOfReceivers.Count; i++)
                {
                    if (i == 0)
                        continue;

                    MailAddress receipent = new MailAddress(strListOfReceivers[i]);
                    message.To.Add(receipent);
                }

                //for (int i = 0; i < strListofCc.Count; i++)
                //{
                //    MailAddress receipent = new MailAddress(strListofCc[i]);
                //    message.CC.Add(receipent);
                //}

                var bodyMail = File.ReadAllText(BodyPath).Replace("\r\n", string.Empty);
                bodyMail = bodyMail.Replace("@UpdateTime", DateTime.Now.ToString("HH:mm:ss"));
                bodyMail = bodyMail.Replace("@URL", URL);
                message.Body = bodyMail;
                message.IsBodyHtml = true;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                string _buSubject = "";
                switch (BU)
                {
                    case "ATT":
                        _buSubject = "Attraction";
                        break;
                    case "RET":
                        _buSubject = "Retail";
                        break;
                    case "DIS":
                        _buSubject = "Distribution";
                        break;
                    default:
                        _buSubject = "";
                        break;
                }
                message.Subject = _buSubject+ " Revenue Reporting -" + DateTime.Now.ToString("dd/MM/yyyy");
                message.SubjectEncoding = System.Text.Encoding.UTF8;

                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectATT));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectRET));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectDIS));
                message.Attachments.Add(new Attachment(FileAttachment));
                client.Send(message);

                message.Dispose();
                Console.WriteLine("Sent email successfully BU-" + _buSubject);


                result = true;
            }
            catch (Exception ex1)
            {
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail Error "+ex1.Message);
                SendErrorMessage("Revenue:" + ex1.Message);
                Thread.Sleep(300000);
                SendEmailByBU(BU, Receivers, BodyPath, URL, FileAttachment);
            }
            if (!result)
            {
                
                Thread.Sleep(300000);
                SendEmailByBU(BU, Receivers, BodyPath,URL,FileAttachment);
            }
        }
        private static void SendEmailTiniPass( List<string> Receivers, string BodyPath,  string FileAttachment)
        {
            bool result = false;
            try
            {

                List<string> strListOfReceivers = Receivers;
                List<string> strListofCc = ConfigurationManager.AppSettings["strListOfCC"].Split('|').ToList();
                string strConfigSender = ConfigurationManager.AppSettings["strSender"];
                string strConfigPassword = ConfigurationManager.AppSettings["strPassword"];
                string strPassword = Encrypt.fDecrypt(strConfigPassword);

                SmtpClient client = new SmtpClient("smtp.office365.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential(strConfigSender, strPassword);
                MailAddress from = new MailAddress(strConfigSender, String.Empty, System.Text.Encoding.UTF8);
                MailAddress to = new MailAddress(strListOfReceivers[0]);
                MailMessage message = new MailMessage(from, to);


                for (int i = 0; i < strListOfReceivers.Count; i++)
                {
                    if (i == 0)
                        continue;

                    MailAddress receipent = new MailAddress(strListOfReceivers[i]);
                    message.To.Add(receipent);
                }

                //for (int i = 0; i < strListofCc.Count; i++)
                //{
                //    MailAddress receipent = new MailAddress(strListofCc[i]);
                //    message.CC.Add(receipent);
                //}

                var bodyMail = File.ReadAllText(BodyPath).Replace("\r\n", string.Empty);
                bodyMail = bodyMail.Replace("@UpdateTime", DateTime.Now.ToString("HH:mm:ss"));
                
                message.Body = bodyMail;
                message.IsBodyHtml = true;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                
                message.Subject ="TiniPass Revenue Reporting -" + DateTime.Now.ToString("dd/MM/yyyy");
                message.SubjectEncoding = System.Text.Encoding.UTF8;

                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectATT));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectRET));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectDIS));
                message.Attachments.Add(new Attachment(FileAttachment));
                client.Send(message);

                message.Dispose();
                Console.WriteLine("Sent email successfully TiniPass");


                result = true;
            }
            catch (Exception ex1)
            {
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail Error " + ex1.Message);
                SendErrorMessage("TiniPass:" + ex1.Message);
                Thread.Sleep(300000);
                SendEmailTiniPass(Receivers, BodyPath, FileAttachment);
            }
            if (!result)
            {
                Thread.Sleep(300000);
                SendEmailTiniPass( Receivers, BodyPath,  FileAttachment);
            }
        }
        private static void SendEmailVolumeAnalytics(List<string> Receivers, string BodyPath, string FileAttachment)
        {
            bool result = false;
            try
            {

                List<string> strListOfReceivers = Receivers;
                List<string> strListofCc = ConfigurationManager.AppSettings["strListOfCC"].Split('|').ToList();
                string strConfigSender = ConfigurationManager.AppSettings["strSender"];
                string strConfigPassword = ConfigurationManager.AppSettings["strPassword"];
                string strPassword = Encrypt.fDecrypt(strConfigPassword);

                SmtpClient client = new SmtpClient("smtp.office365.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential(strConfigSender, strPassword);
                MailAddress from = new MailAddress(strConfigSender, String.Empty, System.Text.Encoding.UTF8);
                MailAddress to = new MailAddress(strListOfReceivers[0]);
                MailMessage message = new MailMessage(from, to);


                for (int i = 0; i < strListOfReceivers.Count; i++)
                {
                    if (i == 0)
                        continue;

                    MailAddress receipent = new MailAddress(strListOfReceivers[i]);
                    message.To.Add(receipent);
                }

                //for (int i = 0; i < strListofCc.Count; i++)
                //{
                //    MailAddress receipent = new MailAddress(strListofCc[i]);
                //    message.CC.Add(receipent);
                //}

                var bodyMail = File.ReadAllText(BodyPath).Replace("\r\n", string.Empty);
                bodyMail = bodyMail.Replace("@UpdateTime", DateTime.Now.ToString("HH:mm:ss"));

                message.Body = bodyMail;
                message.IsBodyHtml = true;
                message.BodyEncoding = System.Text.Encoding.UTF8;

                message.Subject = "Volume Analytics Reporting -" + DateTime.Now.ToString("dd/MM/yyyy");
                message.SubjectEncoding = System.Text.Encoding.UTF8;

                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectATT));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectRET));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectDIS));
                message.Attachments.Add(new Attachment(FileAttachment));
                client.Send(message);

                message.Dispose();
                Console.WriteLine("Sent email successfully TiniPass");


                result = true;
            }
            catch(Exception ex1)
            {
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail Error " + ex1.Message);
                SendErrorMessage("VolumeAnalytics:" + ex1.Message);
                Thread.Sleep(300000);
                SendEmailVolumeAnalytics(Receivers, BodyPath, FileAttachment);
            }
            if (!result)
            {
                Thread.Sleep(300000);
                SendEmailVolumeAnalytics(Receivers, BodyPath, FileAttachment);
            }
        }
        private static void SendEmailTopUpCampaign(List<string> Receivers, string BodyPath, string FileAttachment)
        {
            bool result = false;
            try
            {

                List<string> strListOfReceivers = Receivers;
                List<string> strListofCc = ConfigurationManager.AppSettings["strListOfCC"].Split('|').ToList();
                string strConfigSender = ConfigurationManager.AppSettings["strSender"];
                string strConfigPassword = ConfigurationManager.AppSettings["strPassword"];
                string strPassword = Encrypt.fDecrypt(strConfigPassword);

                SmtpClient client = new SmtpClient("smtp.office365.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential(strConfigSender, strPassword);
                MailAddress from = new MailAddress(strConfigSender, String.Empty, System.Text.Encoding.UTF8);
                MailAddress to = new MailAddress(strListOfReceivers[0]);
                MailMessage message = new MailMessage(from, to);


                for (int i = 0; i < strListOfReceivers.Count; i++)
                {
                    if (i == 0)
                        continue;

                    MailAddress receipent = new MailAddress(strListOfReceivers[i]);
                    message.To.Add(receipent);
                }

                //for (int i = 0; i < strListofCc.Count; i++)
                //{
                //    MailAddress receipent = new MailAddress(strListofCc[i]);
                //    message.CC.Add(receipent);
                //}

                var bodyMail = File.ReadAllText(BodyPath).Replace("\r\n", string.Empty);
                bodyMail = bodyMail.Replace("@UpdateTime", DateTime.Now.ToString("HH:mm:ss"));

                message.Body = bodyMail;
                message.IsBodyHtml = true;
                message.BodyEncoding = System.Text.Encoding.UTF8;

                message.Subject = "TopUp Campaign Reporting -" + DateTime.Now.ToString("dd/MM/yyyy");
                message.SubjectEncoding = System.Text.Encoding.UTF8;

                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectATT));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectRET));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectDIS));
                //message.Attachments.Add(new Attachment(FileAttachment));
                client.Send(message);

                message.Dispose();
                Console.WriteLine("Sent email successfully TopUp Campaign");


                result = true;
            }
            catch(Exception ex1)
            {
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail Error " + ex1.Message);
                SendErrorMessage("TopUpCampaign:" + ex1.Message);
                Thread.Sleep(300000);
                SendEmailTopUpCampaign(Receivers, BodyPath, FileAttachment);
            }
            if (!result)
            {
                Thread.Sleep(300000);
                SendEmailTopUpCampaign(Receivers, BodyPath, FileAttachment);
            }
        }

        private static void SendEmailTrendingRevenueDaily(List<string> Receivers, string BodyPath, string FileAttachment)
        {
            bool result = false;
            try
            {

                List<string> strListOfReceivers = Receivers;
                List<string> strListofCc = ConfigurationManager.AppSettings["strListOfCC"].Split('|').ToList();
                string strConfigSender = ConfigurationManager.AppSettings["strSender"];
                string strConfigPassword = ConfigurationManager.AppSettings["strPassword"];
                string strPassword = Encrypt.fDecrypt(strConfigPassword);

                SmtpClient client = new SmtpClient("smtp.office365.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential(strConfigSender, strPassword);
                MailAddress from = new MailAddress(strConfigSender, String.Empty, System.Text.Encoding.UTF8);
                MailAddress to = new MailAddress(strListOfReceivers[0]);
                MailMessage message = new MailMessage(from, to);


                for (int i = 0; i < strListOfReceivers.Count; i++)
                {
                    if (i == 0)
                        continue;

                    MailAddress receipent = new MailAddress(strListOfReceivers[i]);
                    message.To.Add(receipent);
                }

                //for (int i = 0; i < strListofCc.Count; i++)
                //{
                //    MailAddress receipent = new MailAddress(strListofCc[i]);
                //    message.CC.Add(receipent);
                //}

                var bodyMail = File.ReadAllText(BodyPath).Replace("\r\n", string.Empty);
                bodyMail = bodyMail.Replace("@UpdateTime", DateTime.Now.ToString("HH:mm:ss"));

                message.Body = bodyMail;
                message.IsBodyHtml = true;
                message.BodyEncoding = System.Text.Encoding.UTF8;

                message.Subject = "Trending Revenue Daily Reporting -" + DateTime.Now.ToString("dd/MM/yyyy");
                message.SubjectEncoding = System.Text.Encoding.UTF8;

                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectATT));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectRET));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectDIS));
                message.Attachments.Add(new Attachment(FileAttachment));
                client.Send(message);

                message.Dispose();
                Console.WriteLine("Sent email successfully Trending Revenue Daily");


                result = true;
            }
            catch (Exception ex1)
            {
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail Error " + ex1.Message);
                SendErrorMessage("TrendingRevenueDaily:" + ex1.Message);
                Thread.Sleep(300000);
                SendEmailTrendingRevenueDaily(Receivers, BodyPath, FileAttachment);
            }
            if (!result)
            {
                Thread.Sleep(300000);
                SendEmailTrendingRevenueDaily(Receivers, BodyPath, FileAttachment);
            }
        }

        private static void SendEmailBillAndTraffic(List<string> Receivers, string BodyPath, string FileAttachment)
        {
            bool result = false;
            try
            {

                List<string> strListOfReceivers = Receivers;
                List<string> strListofCc = ConfigurationManager.AppSettings["strListOfCC"].Split('|').ToList();
                string strConfigSender = ConfigurationManager.AppSettings["strSender"];
                string strConfigPassword = ConfigurationManager.AppSettings["strPassword"];
                string strPassword = Encrypt.fDecrypt(strConfigPassword);

                SmtpClient client = new SmtpClient("smtp.office365.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential(strConfigSender, strPassword);
                MailAddress from = new MailAddress(strConfigSender, String.Empty, System.Text.Encoding.UTF8);
                MailAddress to = new MailAddress(strListOfReceivers[0]);
                MailMessage message = new MailMessage(from, to);


                for (int i = 0; i < strListOfReceivers.Count; i++)
                {
                    if (i == 0)
                        continue;

                    MailAddress receipent = new MailAddress(strListOfReceivers[i]);
                    message.To.Add(receipent);
                }

                //for (int i = 0; i < strListofCc.Count; i++)
                //{
                //    MailAddress receipent = new MailAddress(strListofCc[i]);
                //    message.CC.Add(receipent);
                //}

                var bodyMail = File.ReadAllText(BodyPath).Replace("\r\n", string.Empty);
                bodyMail = bodyMail.Replace("@UpdateTime", DateTime.Now.ToString("HH:mm:ss"));

                message.Body = bodyMail;
                message.IsBodyHtml = true;
                message.BodyEncoding = System.Text.Encoding.UTF8;

                message.Subject = "Bill and Traffic Reporting - " + DateTime.Now.ToString("dd/MM/yyyy");
                message.SubjectEncoding = System.Text.Encoding.UTF8;

                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectATT));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectRET));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectDIS));
                message.Attachments.Add(new Attachment(FileAttachment));
                client.Send(message);

                message.Dispose();
                Console.WriteLine("Sent email successfully TiniPass");


                result = true;
            }
            catch (Exception ex1)
            {
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail Error " + ex1.Message);
                SendErrorMessage("BillAndTraffic:" + ex1.Message);
                Thread.Sleep(300000);
                SendEmailBillAndTraffic(Receivers, BodyPath, FileAttachment);
            }
            if (!result)
            {
                Thread.Sleep(300000);
                SendEmailBillAndTraffic(Receivers, BodyPath, FileAttachment);
            }
        }

        private static void SendErrorMessage(string body)
        {
            try
            {
                string strConfigSender = ConfigurationManager.AppSettings["strSender"];
                string strConfigPassword = ConfigurationManager.AppSettings["strPassword"];
                string strPassword = Encrypt.fDecrypt(strConfigPassword);

                SmtpClient client = new SmtpClient("smtp.office365.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential(strConfigSender, strPassword);
                MailAddress from = new MailAddress(strConfigSender, String.Empty, System.Text.Encoding.UTF8);
                MailAddress to = new MailAddress("trai.tran@nkidgroup.com");
                MailMessage message = new MailMessage(from, to);
                message.Body = body;
                message.IsBodyHtml = true;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.Subject = (DateTime.Now.AddDays(-1)).ToString("dd.MM.yyyy") + " Reporting";
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                client.Send(message);
                Console.WriteLine("Sent email successfully");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void SendEmailReport(string Subject, List<string> Receivers, string BodyPath, string FileAttachment)
        {
            bool result = false;
            try
            {

                List<string> strListOfReceivers = Receivers;
                List<string> strListofCc = ConfigurationManager.AppSettings["strListOfCC"].Split('|').ToList();
                string strConfigSender = ConfigurationManager.AppSettings["strSender"];
                string strConfigPassword = ConfigurationManager.AppSettings["strPassword"];
                string strPassword = Encrypt.fDecrypt(strConfigPassword);

                SmtpClient client = new SmtpClient("smtp.office365.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential(strConfigSender, strPassword);
                MailAddress from = new MailAddress(strConfigSender, String.Empty, System.Text.Encoding.UTF8);
                MailAddress to = new MailAddress(strListOfReceivers[0]);
                MailMessage message = new MailMessage(from, to);


                for (int i = 0; i < strListOfReceivers.Count; i++)
                {
                    if (i == 0)
                        continue;

                    MailAddress receipent = new MailAddress(strListOfReceivers[i]);
                    message.To.Add(receipent);
                }

                //for (int i = 0; i < strListofCc.Count; i++)
                //{
                //    MailAddress receipent = new MailAddress(strListofCc[i]);
                //    message.CC.Add(receipent);
                //}

                var bodyMail = File.ReadAllText(BodyPath).Replace("\r\n", string.Empty);
                bodyMail = bodyMail.Replace("@UpdateTime", DateTime.Now.ToString("HH:mm:ss"));

                message.Body = bodyMail;
                message.IsBodyHtml = true;
                message.BodyEncoding = System.Text.Encoding.UTF8;

                message.Subject = Subject + " - " + DateTime.Now.ToString("dd/MM/yyyy");
                message.SubjectEncoding = System.Text.Encoding.UTF8;

                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectATT));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectRET));
                //message.Attachments.Add(new Attachment(excelUtilities.strSourceFullDirectDIS));
                message.Attachments.Add(new Attachment(FileAttachment));
                client.Send(message);

                message.Dispose();
                Console.WriteLine("Sent email successfully "+Subject);


                result = true;
            }
            catch (Exception ex1)
            {
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail Error " + ex1.Message);
                SendErrorMessage(Subject +" - "+ ex1.Message);
                Thread.Sleep(300000);
                SendEmailReport(Subject, Receivers, BodyPath, FileAttachment);
            }
            if (!result)
            {
                Thread.Sleep(300000);
                SendEmailReport(Subject, Receivers, BodyPath, FileAttachment);
            }
        }

        private async void timer1_Tick(object sender, EventArgs e)
        {
            
            if (DateTime.Now.Hour >= fromHours && DateTime.Now.Hour <= toHours)
            {
               
                
                if (DateTime.Now.Hour == 7)
                {
                    SendMailMarketingOnlineSaleReport();
                    SendMailTrendingRevenueReport();
                    SendMailSalesReport();
                    SendMailTiniPassSalesReport();
                    SendMailBillAndTrafficReport();
                    SendMailVolumeAnalyticsReport();
                    SendMailTopUpCampaignReport();
                }
                //if (DateTime.Now.Hour == 10)
                //    await InsertCRMDataJson();
            }
                

        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            var dt_timer= GetTimeStart();
            if (dt_timer != null && dt_timer.Rows.Count>0)
            {
                fromHours = Convert.ToInt32(dt_timer.Rows[0]["FromHours"].ToString());
                toHours = Convert.ToInt32(dt_timer.Rows[0]["ToHours"].ToString());
                _interval = Convert.ToInt32(dt_timer.Rows[0]["Interval"].ToString());
            }
            timer1.Interval = _interval;
            timer1.Enabled = true;

            
            this.WindowState = FormWindowState.Minimized;
            if (DateTime.Now.Hour >= fromHours && DateTime.Now.Hour <= toHours)
            {
                
               
                if (DateTime.Now.Hour == 7)
                {
                    SendMailMarketingOnlineSaleReport();
                    SendMailTrendingRevenueReport();
                    SendMailSalesReport();
                    SendMailTiniPassSalesReport();
                    SendMailBillAndTrafficReport();
                    SendMailVolumeAnalyticsReport();
                    SendMailTopUpCampaignReport();
                }
                //if (DateTime.Now.Hour == 10 )
                //    await InsertCRMDataJson();
            }
                
            
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ShowInTaskbar = true;
            notifyIcon1.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            ShowInTaskbar = false;
            notifyIcon1.Visible = true;
            //notifyIcon1.ShowBalloonTip(1000);
            e.Cancel = true;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                ShowInTaskbar = false;
                notifyIcon1.Visible = true;
                //notifyIcon1.ShowBalloonTip(1000);
            }
        }


        private static void SendMailTopUpCampaignReport()
        {
            string message = string.Empty;
            try
            {


                s3Client = new AmazonS3Client(keyName, secretKey, bucketRegion);

                string strConfigFromDate = ConfigurationManager.AppSettings["strFromDate"];
                string strConfigToDate = ConfigurationManager.AppSettings["strToDate"];

                string rev_template = AppDomain.CurrentDomain.BaseDirectory + @"\TopUp_Campaign_Template.xlsx";

                string rev_temp = AppDomain.CurrentDomain.BaseDirectory + @"\Temp\TopUp_Campaign_Template.xlsx";
                string rev_source = AppDomain.CurrentDomain.BaseDirectory + @"\DataSource\TopUp_Campaign_Template_"+ DateTime.Now.ToString("yyyyMMddHHmmss") +".xlsx";

                rev_template = rev_template.Replace("\\\\", "\\");
                rev_temp = rev_temp.Replace("\\\\", "\\");
                rev_source = rev_source.Replace("\\\\", "\\");


                var curDate = DateTime.Today.AddDays(-1);
                DateTime dFromDate = new DateTime(curDate.Year, curDate.Month, 1);
                DateTime dToDate = DateTime.Today.AddDays(-1);

                if (!string.IsNullOrWhiteSpace(strConfigFromDate))
                    dFromDate = Convert.ToDateTime(strConfigFromDate);

                if (!string.IsNullOrWhiteSpace(strConfigToDate))
                    dToDate = Convert.ToDateTime(strConfigToDate);

                Console.WriteLine("Initializing TopUpCampaign...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Initializing TopUpCampaign...");
                SetExcelUtility(dFromDate, dToDate);

                excelUtilities.KillSpecificExcelFileProcessBackgroud();

                excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                if (File.Exists(rev_temp))
                    File.Delete(rev_temp);
                excelUtilities.KillSpecificExcelFileProcess(rev_source);
                if (File.Exists(rev_source))
                    File.Delete(rev_source);
                excelUtilities.KillSpecificExcelFileProcess("");



                dFromDate = new DateTime(curDate.Year, 8, 1);
                dToDate = new DateTime(curDate.Year, 12, 31);

                Console.WriteLine("Getting Data TopUpCampaign...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Getting Data TopUpCampaign...");
                
                var dtTopUpCampaign = GetDataGet_TopUp_Game(dFromDate, dToDate);
                var dtSpending = GetDataGet_Spending_By_CardNo(dFromDate, dToDate);
                var dtMembership = GetDataGet_Membership(dFromDate, dToDate);
                var dtRawSpending = GetDataGet_RawSpending(dFromDate, dToDate);

                Console.WriteLine("Removing Sheet Raw Data TopUpCampaign...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Removing Sheet Raw Data TopUpCampaign...");
                excelUtilities.RemoveRange(new List<string>() { "TopUpGame_RowData", "Spending by CardNo" , "Membership" , "RawSpending" }, rev_template);

                Console.WriteLine("Insert Data TopUpCampaign...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Insert Data TopUpCampaign...");
                //excelUtilities.WriteData(dtResults, sheetArr, excelUtilities.strDesFullDirect, 1, 2, dToDate);

                excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                excelUtilities.WriteDataToUpCampaign(dtSpending.Tables[0], "Spending by CardNo", 1, 1, rev_temp, false);
                excelUtilities.WriteDataToUpCampaign(dtMembership.Tables[0], "Membership", 1, 1, rev_temp, false);
                excelUtilities.WriteDataToUpCampaign(dtRawSpending.Tables[0], "RawSpending", 1, 1, rev_temp, false);

                excelUtilities.WriteDataToUpCampaign(dtTopUpCampaign.Tables[0], "TopUpGame_RowData", 1, 1, rev_temp, true);
                

                excelUtilities.HideSheet(rev_temp, "Spending by CardNo");
                excelUtilities.HideSheet(rev_temp, "Membership");
                excelUtilities.HideSheet(rev_temp, "TopUpGame_RowData");
                excelUtilities.HideSheet(rev_temp, "RawSpending");

                //Console.WriteLine("Refresh Pivot Data...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Refresh Pivot Data TopUpCampaign...");
                //excelUtilities.RefreshSheetsTopUpCampaign(dtTopUpCampaign.Tables[0], "Spending", "TopUpGame_RowData", rev_temp, true);

                excelUtilities.RefreshSheetsTopUpCampaign(dtTopUpCampaign.Tables[0], "Topup amount by center - Daily", "TopUpGame_RowData", rev_temp, true);


                excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                excelUtilities.KillSpecificExcelFileProcess("");
                File.Copy(rev_temp, rev_source);
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Begin Upload file ...");
                UploadFileNkidCloud(rev_source);
                //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-End Upload file...");
                Console.WriteLine("Send Mail TopUpCampaign...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail TopUpCampaign...");
                SendEmailTopUpCampaign(new List<string>() { "1f0fd33d.nkidgroup.com@apac.teams.ms" }, AppDomain.CurrentDomain.BaseDirectory + @"\Body_Topup_Campaign.html", null);

                if (File.Exists(rev_source))
                    File.Delete(rev_source);
                excelUtilities.KillSpecificExcelFileProcess("");

                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Done TopUpCampaign...");
                WriteLog("-------###-------");





            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                message = ex.Message;
            }

            if (!string.IsNullOrWhiteSpace(message))
                SendErrorMessage(message);

            //Console.ReadLine();

        }

        private static void SendMailTrendingRevenueReport()
        {
            string message = string.Empty;
            try
            {


                s3Client = new AmazonS3Client(keyName, secretKey, bucketRegion);

                string strConfigFromDate = ConfigurationManager.AppSettings["strFromDate"];
                string strConfigToDate = ConfigurationManager.AppSettings["strToDate"];

                string rev_template = AppDomain.CurrentDomain.BaseDirectory + @"\Trending_Revenue_Daily_Template.xlsx";

                string rev_temp = AppDomain.CurrentDomain.BaseDirectory + @"\Temp\Trending_Revenue_Daily_Template.xlsx";
                string rev_source = AppDomain.CurrentDomain.BaseDirectory + @"\DataSource\Trending_Revenue_Daily_Template.xlsx";

                rev_template = rev_template.Replace("\\\\", "\\");
                rev_temp = rev_temp.Replace("\\\\", "\\");
                rev_source = rev_source.Replace("\\\\", "\\");


                var curDate = DateTime.Today.AddDays(-1);
                DateTime dFromDate = new DateTime(curDate.Year, curDate.Month, 1);
                DateTime dToDate = DateTime.Today.AddDays(-1);

                if (!string.IsNullOrWhiteSpace(strConfigFromDate))
                    dFromDate = Convert.ToDateTime(strConfigFromDate);

                if (!string.IsNullOrWhiteSpace(strConfigToDate))
                    dToDate = Convert.ToDateTime(strConfigToDate);

                Console.WriteLine("Initializing TrendingRevenue...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Initializing TrendingRevenue...");
                SetExcelUtility(dFromDate, dToDate);

                excelUtilities.KillSpecificExcelFileProcessBackgroud();

                excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                if (File.Exists(rev_temp))
                    File.Delete(rev_temp);
                excelUtilities.KillSpecificExcelFileProcess(rev_source);
                if (File.Exists(rev_source))
                    File.Delete(rev_source);
                excelUtilities.KillSpecificExcelFileProcess("");



                dFromDate = new DateTime(curDate.Year, 8, 1);
                dToDate = new DateTime(curDate.Year, 10, 31);

                Console.WriteLine("Getting Data TrendingRevenue...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Getting Data TrendingRevenue...");

                var dtTrending_Revenue = GetData_Trending_Revenue_Daily();
               

                Console.WriteLine("Removing Sheet Raw Data TrendingRevenue...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Removing Sheet Raw Data TrendingRevenue...");
                excelUtilities.RemoveRange(new List<string>() { "RAW_DATA" }, rev_template);

                Console.WriteLine("Insert Data TrendingRevenue...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Insert Data TrendingRevenue...");
                //excelUtilities.WriteData(dtResults, sheetArr, excelUtilities.strDesFullDirect, 1, 2, dToDate);

                excelUtilities.KillSpecificExcelFileProcess(rev_temp);


                excelUtilities.WriteDataTrendingRevenueDaily(dtTrending_Revenue.Tables[0], "RAW_DATA", 1, 1, rev_temp, true);


                excelUtilities.HideSheet(rev_temp, "RAW_DATA");
               

                ////Console.WriteLine("Refresh Pivot Data...");
                //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Refresh Pivot Data...");
               
                //excelUtilities.RefreshSheetsTopUpCampaign(dtTopUpCampaign.Tables[0], "Topup amount by center - Daily", "TopUpGame_RowData", rev_temp, true);


                excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                excelUtilities.KillSpecificExcelFileProcess("");
                File.Copy(rev_temp, rev_source);
                //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Begin Upload file ...");
                //UploadFile(rev_source);
                //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-End Upload file...");
                Console.WriteLine("Send Mail TrendingRevenue...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail TrendingRevenue...");
                SendEmailTrendingRevenueDaily(new List<string>() { "c60c36d5.nkidgroup.com@apac.teams.ms" }, AppDomain.CurrentDomain.BaseDirectory + @"\Body_Trending_Revenue_Daily.html", rev_source);


                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Done TrendingRevenue...");
                WriteLog("-------###-------");





            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                message = ex.Message;
            }

            if (!string.IsNullOrWhiteSpace(message))
                SendErrorMessage(message);

            //Console.ReadLine();

        }

        private static void SendMailVolumeAnalyticsReport()
        {
            string message = string.Empty;
            try
            {
                

                    s3Client = new AmazonS3Client(keyName, secretKey, bucketRegion);

                    string strConfigFromDate = ConfigurationManager.AppSettings["strFromDate"];
                    string strConfigToDate = ConfigurationManager.AppSettings["strToDate"];

                    string rev_template = AppDomain.CurrentDomain.BaseDirectory + @"\Volume_Analyse_Report.xlsx";

                    string rev_temp = AppDomain.CurrentDomain.BaseDirectory + @"\Temp\Volume_Analyse_Report.xlsx";
                    string rev_source = AppDomain.CurrentDomain.BaseDirectory + @"\DataSource\Volume_Analyse_Report.xlsx";

                    rev_template = rev_template.Replace("\\\\", "\\");
                    rev_temp = rev_temp.Replace("\\\\", "\\");
                    rev_source = rev_source.Replace("\\\\", "\\");


                    var curDate = DateTime.Today.AddDays(-1);
                    DateTime dFromDate = new DateTime(curDate.Year, curDate.Month, 1);
                    DateTime dToDate = DateTime.Today.AddDays(-1);

                    if (!string.IsNullOrWhiteSpace(strConfigFromDate))
                        dFromDate = Convert.ToDateTime(strConfigFromDate);

                    if (!string.IsNullOrWhiteSpace(strConfigToDate))
                        dToDate = Convert.ToDateTime(strConfigToDate);

                    Console.WriteLine("Initializing VolumeAnalytics...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Initializing VolumeAnalytics...");
                    SetExcelUtility(dFromDate, dToDate);

                    excelUtilities.KillSpecificExcelFileProcessBackgroud();

                    excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                    if (File.Exists(rev_temp))
                        File.Delete(rev_temp);
                    excelUtilities.KillSpecificExcelFileProcess(rev_source);
                    if (File.Exists(rev_source))
                        File.Delete(rev_source);
                    excelUtilities.KillSpecificExcelFileProcess("");





                    Console.WriteLine("Getting Data VolumeAnalytics...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Getting Data VolumeAnalytics...");
                    var dtFreeTicket = GetDataFreeTicketVolumeReport();
                    var dtResults_All = GetDataDailyTiniPassAllSalesReport();
                    var dtDiscountTicket = GetDataDiscountTicketVolumeReport();
                    var dtCardLevel = GetDataCardLevelVolumeReport();
                    var dtInvitationTicket = GetDataInvitationTicketReport();

                    Console.WriteLine("Removing Sheet Raw Data VolumeAnalytics...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Removing Sheet Raw Data VolumeAnalytics...");
                    excelUtilities.RemoveRange(new List<string>() { "InvitationTicket", "TP600CardLevel", "FreeTicket","DiscountTicket", "ALL_RAW_DATA" }, rev_template);

                    Console.WriteLine("Insert Data VolumeAnalytics...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Insert Data VolumeAnalytics...");
                    //excelUtilities.WriteData(dtResults, sheetArr, excelUtilities.strDesFullDirect, 1, 2, dToDate);

                    excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                    excelUtilities.WriteDataVolumeAnalytics(dtFreeTicket.Tables[0], "FreeTicket", 1, 1, rev_temp, false);
                    excelUtilities.WriteDataVolumeAnalytics(dtInvitationTicket.Tables[0], "InvitationTicket", 1, 1, rev_temp, false);
                    excelUtilities.WriteDataVolumeAnalytics(dtDiscountTicket.Tables[0], "DiscountTicket", 1, 1, rev_temp, false);
                    excelUtilities.WriteDataVolumeAnalytics(dtCardLevel.Tables[0], "TP600CardLevel", 1, 1, rev_temp, false);

                    excelUtilities.WriteDataVolumeAnalytics(dtResults_All.Tables[0], "ALL_RAW_DATA", 1, 1, rev_temp, true);

                    excelUtilities.HideSheet(rev_temp, "TP600CardLevel");
                    excelUtilities.HideSheet(rev_temp, "FreeTicket");
                    excelUtilities.HideSheet(rev_temp, "ALL_RAW_DATA");
                    excelUtilities.HideSheet(rev_temp, "DiscountTicket");
                    excelUtilities.HideSheet(rev_temp, "InvitationTicket");

                    //Console.WriteLine("Refresh Pivot Data...");

                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Refresh Pivot Data VolumeAnalytics...");
                    excelUtilities.RefreshSheetsTiniPass(dtCardLevel.Tables[0], "Pass Sold By Card Level Daily", "TP600CardLevel", rev_temp, true);
                    
                    //excelUtilities.RefreshSheetsTiniPass(dtResults.Tables[0], "tiNiPass", "RAW_DATA", rev_temp, true);

                    excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                    excelUtilities.KillSpecificExcelFileProcess("");
                    File.Copy(rev_temp, rev_source);
                    //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Begin Upload file ...");
                    //UploadFile(rev_source);
                    //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-End Upload file...");
                    Console.WriteLine("Send Mail VolumeAnalytics...");
                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail VolumeAnalytics...");
                    SendEmailVolumeAnalytics(new List<string>() { "f10978cc.nkidgroup.com@apac.teams.ms" }, AppDomain.CurrentDomain.BaseDirectory + @"\Body_Volume_Analyse.html", rev_source);


                    WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Done VolumeAnalytics...");
                    WriteLog("-------###-------");



               

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                message = ex.Message;
            }

            if (!string.IsNullOrWhiteSpace(message))
                SendErrorMessage(message);

            //Console.ReadLine();

        }


        private static void SendMailMarketingOnlineSaleReport()
        {
            string message = string.Empty;
            try
            {


                s3Client = new AmazonS3Client(keyName, secretKey, bucketRegion);

                string strConfigFromDate = ConfigurationManager.AppSettings["strFromDate"];
                string strConfigToDate = ConfigurationManager.AppSettings["strToDate"];

                string rev_template = AppDomain.CurrentDomain.BaseDirectory + @"\Marketing_Online_Sale_Template.xlsx";

                string rev_temp = AppDomain.CurrentDomain.BaseDirectory + @"\Temp\Marketing_Online_Sale_Template.xlsx";
                string rev_source = AppDomain.CurrentDomain.BaseDirectory + @"\DataSource\Marketing_Online_Sale_Template.xlsx";

                rev_template = rev_template.Replace("\\\\", "\\");
                rev_temp = rev_temp.Replace("\\\\", "\\");
                rev_source = rev_source.Replace("\\\\", "\\");


                var curDate = DateTime.Today.AddDays(-1);
                DateTime dFromDate = new DateTime(curDate.Year, curDate.Month, 1);
                DateTime dToDate = DateTime.Today.AddDays(-1);

                if (!string.IsNullOrWhiteSpace(strConfigFromDate))
                    dFromDate = Convert.ToDateTime(strConfigFromDate);

                if (!string.IsNullOrWhiteSpace(strConfigToDate))
                    dToDate = Convert.ToDateTime(strConfigToDate);

                Console.WriteLine("Initializing Marketing Online Sale...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Initializing Marketing Online Sale...");
                SetExcelUtility(dFromDate, dToDate);

                excelUtilities.KillSpecificExcelFileProcessBackgroud();

                excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                if (File.Exists(rev_temp))
                    File.Delete(rev_temp);
                excelUtilities.KillSpecificExcelFileProcess(rev_source);
                if (File.Exists(rev_source))
                    File.Delete(rev_source);
                excelUtilities.KillSpecificExcelFileProcess("");



                dFromDate = new DateTime(curDate.Year, 8, 1);
                dToDate = new DateTime(curDate.Year, 12, 31);

                Console.WriteLine("Getting Data Marketing Online Sale...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Getting Data Marketing Online Sale...");

                string storename = "[115.75.6.184].[ANALYTICS_POS_DATA].[dbo].USP_HAR_CustomerList";
                Hashtable storeParams = new Hashtable();
                //storeParams.Add("FromDate", dFromDate.ToString("yyyyMMdd"));
                var dtCustomerList = GetDataReport(storename, storeParams);


                storename = "[115.75.6.184].[ANALYTICS_POS_DATA].[dbo].USP_HAR_Marketing_Tracking";
                storeParams = new Hashtable();
                var dtHAR_Marketing_Tracking = GetDataReport(storename, storeParams);
                
                storename = "[115.75.6.184].[ANALYTICS_POS_DATA].[dbo].USP_HAR_Marketing_Tracking_Daily";
                storeParams = new Hashtable();
                var dttiNiListHAR_Marketing_Tracking_Daily = GetDataReport(storename, storeParams);
                
                storename = "[115.75.6.184].[ANALYTICS_POS_DATA].[dbo].USP_HAR_Marketing_Tracking_Weekly";
                storeParams = new Hashtable();
                var dttiNiListHAR_Marketing_Tracking_Weekly = GetDataReport(storename, storeParams);
               
                storename = "[115.75.6.184].[ANALYTICS_POS_DATA].[dbo].USP_HAR_Marketing_Tracking_Monthly";
                storeParams = new Hashtable();
                var dtHAR_Marketing_Tracking_Monthly = GetDataReport(storename, storeParams);

                storename = "[115.75.6.184].[ANALYTICS_POS_DATA].[dbo].USP_HAR_Marketing_Tracking_Bi_Monthly";
                storeParams = new Hashtable();
                var dtHAR_Marketing_Tracking_Bi_Monthly = GetDataReport(storename, storeParams);
               

                Console.WriteLine("Removing Sheet Raw Data Marketing Online Sale...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Removing Sheet Raw Data Marketing Online Sale...");
                excelUtilities.RemoveRange(new List<string>() { "CustomerList", "RAW_DATA", "Daily", "Weekly", "Monthly", "Bi-Monthly" }, rev_template,5,1);

                Console.WriteLine("Insert Data Marketing Online Sale...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Insert Data Marketing Online Sale...");
                //excelUtilities.WriteData(dtResults, sheetArr, excelUtilities.strDesFullDirect, 1, 2, dToDate);

                excelUtilities.KillSpecificExcelFileProcess(rev_temp);



                excelUtilities.WriteRangeData(rev_template, new List<WriteData>()
                {
                    new WriteData() { Datasource= dtCustomerList.Tables[0], SheetName="CustomerList",StartColumn=1,StartRow=5,SaveFileName=rev_temp,IsSaveFile=false},
                    new WriteData() { Datasource= dttiNiListHAR_Marketing_Tracking_Daily.Tables[0], SheetName="Daily",StartColumn=1,StartRow=5,SaveFileName=rev_temp,IsSaveFile=false},
                    new WriteData() { Datasource= dttiNiListHAR_Marketing_Tracking_Weekly.Tables[0], SheetName="Weekly",StartColumn=1,StartRow=5,SaveFileName=rev_temp,IsSaveFile=false},
                    new WriteData() { Datasource= dtHAR_Marketing_Tracking_Monthly.Tables[0], SheetName="Monthly",StartColumn=1,StartRow=5,SaveFileName=rev_temp,IsSaveFile=false},
                    new WriteData() { Datasource= dtHAR_Marketing_Tracking_Bi_Monthly.Tables[0], SheetName="Bi-Monthly",StartColumn=1,StartRow=5,SaveFileName=rev_temp,IsSaveFile=true},
                    new WriteData() { Datasource= dtHAR_Marketing_Tracking.Tables[0], SheetName="RAW_DATA",StartColumn=1,StartRow=1,SaveFileName=rev_temp,IsSaveFile=false}
                });


                //excelUtilities.WriteDataRevenuePNL(dtRevenue_COGS.Tables[0], "Revenue & COGS", 1, 1, rev_temp, false);
                //excelUtilities.WriteDataRevenuePNL(dtCash.Tables[0], "CASH", 1, 1, rev_temp, false);
                //excelUtilities.WriteDataRevenuePNL(dttiNiPass.Tables[0], "tiNiPass", 1, 1, rev_temp, false);
                //excelUtilities.WriteDataRevenuePNL(dttiNiList.Tables[0], "tiNiList", 1, 1, rev_temp, false);
                //excelUtilities.WriteDataRevenuePNL(dtRETNoOfBills.Tables[0], "RET No. of bills", 1, 1, rev_temp, false);
                //excelUtilities.WriteDataRevenuePNL(dtRETInventory.Tables[0], "RET Inventory", 1, 1, rev_temp, false);
                //excelUtilities.WriteDataRevenuePNL(dtATTVolume.Tables[0], "ATT Volume", 1, 1, rev_temp, true);
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "Hide Sheet Marketing Online Sale...");

                excelUtilities.HideRangeSheet(rev_temp, new List<string>() { "CustomerList", "RAW_DATA",  "List" });

                //excelUtilities.HideSheet(rev_temp, "Revenue & COGS");
                //excelUtilities.HideSheet(rev_temp, "CASH");
                //excelUtilities.HideSheet(rev_temp, "tiNiPass");
                //excelUtilities.HideSheet(rev_temp, "tiNiList");
                //excelUtilities.HideSheet(rev_temp, "RET No. of bills");
                //excelUtilities.HideSheet(rev_temp, "RET Inventory");
                //excelUtilities.HideSheet(rev_temp, "ATT Volume");
                //excelUtilities.HideSheet(rev_temp, "COL");
                //excelUtilities.HideSheet(rev_temp, "List");
                //excelUtilities.HideSheet(rev_temp, "tiNiPass Rev. Forecast");
                //excelUtilities.HideSheet(rev_temp, "Rent & Rev. Forecast");


                //////Console.WriteLine("Refresh Pivot Data...");
                ////WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Refresh Pivot Data...");
                ////excelUtilities.RefreshSheetsTopUpCampaign(dtTopUpCampaign.Tables[0], "Spending", "TopUpGame_RowData", rev_temp, true);

                excelUtilities.KillSpecificExcelFileProcess(rev_temp);
                excelUtilities.KillSpecificExcelFileProcess("");
                File.Copy(rev_temp, rev_source);
                //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Begin Upload file ...");
                //UploadFile(rev_source);
                //WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-End Upload file...");
                Console.WriteLine("Send Mail Marketing Online Sale...");
                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Send Mail Marketing Online Sale...");
                SendEmailReport("Marketing Online Sale Reporting", new List<string>() { "8187a4be.nkidgroup.com@apac.teams.ms" }, AppDomain.CurrentDomain.BaseDirectory + @"\Body_Marketing_Online_Sale_Report.html", rev_source);


                WriteLog(DateTime.Now.TimeOfDay.Ticks.ToString() + "-Done Marketing Online Sale...");
                WriteLog("-------###-------");





            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                message = ex.Message;
            }

            if (!string.IsNullOrWhiteSpace(message))
                SendErrorMessage(message);

            //Console.ReadLine();

        }


        static async Task InsertCRMData()
        {
            try
            {
                using (var con = new SqlConnection(conectionString))
                {
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    repositoryCRMData = new DPGenericRepository<CRMData>(con);
                    List<CRMData> _lsCRM = lsCRM.GroupBy(p => new { p.id })
                                                                 .Select(g => g.First())
                                                                 .ToList();
                    await repositoryCRMData.AddAsync(_lsCRM);
                    
                    WriteLog(string.Format("Done Insert CRMData: {0} rows", _lsCRM.Count));

                }
            }
            catch (Exception ex)
            {

                WriteLog(string.Format("Error Insert CRMData {0}", ex.Message));

            }
            
        }
        static async Task InsertPOSInfo()
        {
            try
            {
                using (var con = new SqlConnection(conectionString))
                {
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    repositoryPOSInfo = new DPGenericRepository<POSInfo>(con);
                    List<POSInfo> _lsPI = lsPosInfo.GroupBy(p => new { p.id})
                                                                 .Select(g => g.First())
                                                                 .ToList();
                    await repositoryPOSInfo.AddAsync(_lsPI);

                    WriteLog(string.Format("Done Insert POSInfo: {0} rows", _lsPI.Count));

                }
            }
            catch (Exception ex)
            {

                WriteLog(string.Format("Error Insert POSInfo {0}", ex.Message));

            }
            
        }
        static void DeleteDataCRM()
        {
            try
            {
                string sql = "EXEC USP_DELETE_CRMDATA";
                using (var con = new SqlConnection(conectionString))
                {
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    con.Query(sql);
                }

            }
            catch (Exception ex)
            {
                WriteLog(string.Format("Error Delete CRMData {0}", ex.Message));


            }


        }

        static async Task InsertOrderDataHeader()
        {
            try
            {
                using (var con = new SqlConnection(conectionString))
                {
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    repositoryOrderDataHeader = new DPGenericRepository<OrderDataHeader>(con);

                    List<OrderDataHeader> _lsHD = lsOrderDataHeader.GroupBy(p => new { p.id, p.sourceTransactionId })
                                                                 .Select(g => g.First())
                                                                 .ToList();

                    await repositoryOrderDataHeader.AddAsync(_lsHD);
                    WriteLog(string.Format("Done Insert OrderDataHeader: {0} rows", _lsHD.Count));

                }
            }
            catch (Exception ex)
            {

                WriteLog(string.Format("Error Insert OrderDataHeader {0}", ex.Message));

            }
            
        }
        static async Task InsertOrderDataDetail()
        {
            try
            {
                using (var con = new SqlConnection(conectionString))
                {
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    repositoryOrderDataDetail = new DPGenericRepository<OrderDataDetails>(con);
                    
                    List<OrderDataDetails> _ls = lsOrderDataDetail.GroupBy(p => new { p.id, p.sourceTransactionId, p.itemCode })
                                                                 .Select(g => g.First())
                                                                 .ToList();

                    await repositoryOrderDataDetail.AddAsync(_ls);
                    WriteLog(string.Format("Done Insert OrderDataDetail: {0} rows", _ls.Count));

                }
            }
            catch (Exception ex)
            {

                WriteLog(string.Format("Error Insert tiNiPassEcode {0}", ex.Message));

            }
            
        }
        static async Task InsertTiniPassEcode()
        {
            try
            {
                using (var con = new SqlConnection(conectionString))
                {
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    repositorytiNiPassEcode = new DPGenericRepository<tiNiPassEcode>(con);
                    List<tiNiPassEcode> _lsEc = lstiNiPassEcode.GroupBy(p => new { p.id, p.sourceTransactionId, p.Ecode })
                                                                .Select(g => g.First())
                                                                .ToList();

                    await repositorytiNiPassEcode.AddAsync(_lsEc);
                    WriteLog(string.Format("Done Insert tiNiPassEcode: {0} rows", _lsEc.Count));

                }
            }
            catch (Exception ex)
            {

                WriteLog(string.Format("Error Insert tiNiPassEcode {0}", ex.Message));

            }
           
        }
        public async Task DownloadFile()
        {
            WriteLog("Start download JSON AWS CRMData");
            Dictionary <DateTime, AWS_Info> dc = new Dictionary<DateTime, AWS_Info>();
            using (var client = new AmazonS3Client(keyName, secretKey, bucketRegion))
            {
                var response =  client.ListObjects("nkid-stepfunction-logs", "");
                foreach(var obj in response.S3Objects)
                {
                    string filename = "sfnstatemachineF6E03D88-8pQhd2C5X45L_logs_final.json";

                    if (obj.Size>0 && obj.Key== filename)
                    {
                        DateTime dt = Convert.ToDateTime(obj.LastModified.Date);
                       
                        if (!dc.ContainsKey(dt))
                        {
                            AWS_Info f = new AWS_Info();
                            f.FileName = obj.Key;
                            f.FileSize = obj.Size;
                            f.LastChange = obj.LastModified;

                            dc.Add(dt, f);

                        }
                        else
                        {
                            AWS_Info ai = dc[dt];
                            if (obj.LastModified > ai.LastChange)
                            {
                                dc.Remove(dt);
                                AWS_Info f = new AWS_Info();
                                f.FileName = obj.Key;
                                f.FileSize = obj.Size;
                                f.LastChange = obj.LastModified;

                                dc.Add(dt, f);
                            }
                        }
                        
                        
                    }
                   
                }
                foreach(AWS_Info ai in dc.Values)
                {
                    TransferUtility fileTransferUtility = new TransferUtility(new AmazonS3Client(keyName, secretKey, bucketRegion));

                    // Note the 'fileName' is the 'key' of the object in S3 (which is usually just the file name)
                    string path = AppDomain.CurrentDomain.BaseDirectory + @"\AWS\";
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                    if (File.Exists(path + ai.FileName))
                        File.Delete(path + ai.FileName);

                    filepathAWS = path + ai.FileName;

                    fileTransferUtility.Download(path+ai.FileName, "nkid-stepfunction-logs", ai.FileName);
                    WriteLog("Done download JSON AWS CRMData");
                }
                
            }
           
        }

        static string conectionString = "Data Source=115.75.6.184;Initial Catalog=ANALYTICS_CRM_DATA;User ID=support02;PWD=Nkid@Sap";
        //static string conectionString = "Data Source=210.211.116.19;Initial Catalog=SAP_NKID_CORP;User ID=support02;PWD=Nkid@Sap";

        //static SqlConnection connection;
        static DPGenericRepository<CRMData> repositoryCRMData;
        static DPGenericRepository<POSInfo> repositoryPOSInfo;
        static DPGenericRepository<OrderDataHeader> repositoryOrderDataHeader;
        static DPGenericRepository<OrderDataDetails> repositoryOrderDataDetail;
        static DPGenericRepository<tiNiPassEcode> repositorytiNiPassEcode;

        static List<CRMData> lsCRM;//= new List<CRMData>();
        static List<POSInfo> lsPosInfo;//= new List<POSInfo>();
        static List<OrderDataHeader> lsOrderDataHeader;// = new List<OrderDataHeader>();
        static List<OrderDataDetails> lsOrderDataDetail;// = new List<OrderDataDetails>();
        static List<tiNiPassEcode> lstiNiPassEcode;//= new List<tiNiPassEcode>();


        public async Task<bool> InsertCRMDataJson()
        {
            string input = "";
            try
            {
                WriteLog("############# CRMData JSON ##########");
                await DownloadFile();

                lsPosInfo = new List<POSInfo>();
                lsCRM = new List<CRMData>();
                lsOrderDataHeader = new List<OrderDataHeader>();
                lsOrderDataDetail = new List<OrderDataDetails>();
                lstiNiPassEcode = new List<tiNiPassEcode>();

               
                
                if (File.Exists(filepathAWS))
                {
                    var myJsonString = File.ReadAllText(filepathAWS);
                    dynamic myJObject = JsonConvert.DeserializeObject<List<Fake>>(myJsonString);

                    foreach (Fake fk in myJObject)
                    {
                        input = fk.stateEnteredEventDetails.input;
                        JObject json = JObject.Parse(fk.stateEnteredEventDetails.input.Replace("detail-type", "detailtype"));
                        base_main_new m_b = new base_main_new();
                        base_main m_b1 = new base_main();
                        try
                        {
                            m_b = Newtonsoft.Json.JsonConvert.DeserializeObject<base_main_new>(json.ToString());

                        }
                        catch (Exception)
                        {
                           
                            m_b1 = Newtonsoft.Json.JsonConvert.DeserializeObject<base_main>(json.ToString());

                        }
                        
                        

                        //if (m_b.detail.orderData.sourceTransactionId != "billnumberxxx")
                        //{

                        if(m_b.id!=null)
                        {
                            CRMData crm = new CRMData();
                            crm.account = m_b.account;
                            crm.detailtype = m_b.detailtype;
                            crm.id = m_b.id;
                            crm.time = m_b.time;
                            crm.region = m_b.region;

                            lsCRM.Add(crm);

                            POSInfo inf = new POSInfo();
                            inf.id = m_b.id;
                            inf.role = m_b.detail.sourceSystem.posInfo.role;
                            inf.storecode = (m_b.detail.sourceSystem.name == "online" && string.IsNullOrEmpty(m_b.detail.sourceSystem.posInfo.store_code)) ? "TS-PS053" : m_b.detail.sourceSystem.posInfo.store_code;
                            inf.username = m_b.detail.sourceSystem.posInfo.username;
                            inf.fullname = m_b.detail.sourceSystem.posInfo.fullname;
                            inf.type = m_b.detail.sourceSystem.posInfo.type;
                            lsPosInfo.Add(inf);

                            OrderDataHeader orderH = new OrderDataHeader();
                            orderH.id = m_b.id;
                            orderH.sourceTransactionId = m_b.detail.orderData.sourceTransactionId;
                            orderH.orderDate = m_b.detail.orderData.orderDate;
                            orderH.nfcCardNumber = m_b.detail.orderData.shipTo.nfcCardNumber;
                            orderH.nfcCardType = m_b.detail.orderData.shipTo.nfcCardType;
                            orderH.tinizenCustomerId = m_b.detail.orderData.shipTo.tinizenCustomerId;
                            orderH.customername = m_b.detail.orderData.shipTo.name;
                            orderH.phoneNumber = m_b.detail.orderData.shipTo.phoneNumber;
                            orderH.sendEmail = m_b.detail.orderData.shipTo.sendEmail;
                            lsOrderDataHeader.Add(orderH);

                            var items =
                            from item in m_b.detail.orderData.items
                            group item by new { item.sourceItemId, item.itemcode } into itemGroup
                            select new
                            {
                                itemcode = itemGroup.Key.itemcode,
                                sourceItemId = itemGroup.Key.sourceItemId,
                                quantity = itemGroup.Sum(x => x.quantity),
                            };

                            foreach (var it in items)
                            {
                                OrderDataDetails oD = new OrderDataDetails();
                                oD.id = m_b.id;
                                oD.sourceTransactionId = m_b.detail.orderData.sourceTransactionId;
                                oD.itemCode = it.itemcode;
                                oD.quantity = it.quantity;
                                if (!lsOrderDataDetail.Contains(oD))
                                    lsOrderDataDetail.Add(oD);
                            }

                            foreach (var it in m_b.detail.eCodes.Payload.data)
                            {
                                foreach (var it600 in it.TP600)
                                {
                                    tiNiPassEcode oD = new tiNiPassEcode();
                                    oD.id = m_b.id;
                                    oD.sourceTransactionId = m_b.detail.orderData.sourceTransactionId;
                                    oD.Ecode = it600;
                                    oD.status = m_b.detail.eCodes.Payload.status;
                                    if(!lstiNiPassEcode.Contains(oD))
                                         lstiNiPassEcode.Add(oD);
                                }

                            }
                        }
                        if (m_b1.id!=null)
                        {
                            CRMData crm = new CRMData();
                            crm.account = m_b1.account;
                            crm.detailtype = m_b1.detailtype;
                            crm.id = m_b1.id;
                            crm.time = m_b1.time;
                            crm.region = m_b1.region;

                            lsCRM.Add(crm);

                            POSInfo inf = new POSInfo();
                            inf.id = m_b1.id;
                            inf.role = m_b1.detail.sourceSystem.posInfo.role;
                            inf.storecode = (m_b1.detail.sourceSystem.name == "online" && string.IsNullOrEmpty(m_b1.detail.sourceSystem.posInfo.store_code)) ? "TS-PS053" : m_b1.detail.sourceSystem.posInfo.store_code;
                            inf.username = m_b1.detail.sourceSystem.posInfo.username;
                            inf.fullname = m_b1.detail.sourceSystem.posInfo.fullname;
                            inf.type = m_b1.detail.sourceSystem.posInfo.type;
                            lsPosInfo.Add(inf);

                            OrderDataHeader orderH = new OrderDataHeader();
                            orderH.id = m_b1.id;
                            orderH.sourceTransactionId = m_b1.detail.orderData.sourceTransactionId;
                            orderH.orderDate = m_b1.detail.orderData.orderDate;
                            orderH.nfcCardNumber = m_b1.detail.orderData.shipTo.nfcCardNumber;
                            orderH.nfcCardType = m_b1.detail.orderData.shipTo.nfcCardType;
                            orderH.tinizenCustomerId = m_b1.detail.orderData.shipTo.tinizenCustomerId;
                            orderH.customername = m_b1.detail.orderData.shipTo.name;
                            orderH.phoneNumber = m_b1.detail.orderData.shipTo.phoneNumber;
                            orderH.sendEmail = m_b1.detail.orderData.shipTo.sendEmail;
                            lsOrderDataHeader.Add(orderH);

                            var items =
                            from item in m_b1.detail.orderData.items
                            group item by new { item.sourceItemId, item.itemcode } into itemGroup
                            select new
                            {
                                itemcode = itemGroup.Key.itemcode,
                                sourceItemId = itemGroup.Key.sourceItemId,
                                quantity = itemGroup.Sum(x => x.quantity),
                            };

                            foreach (var it in items)
                            {
                                OrderDataDetails oD = new OrderDataDetails();
                                oD.id = m_b1.id;
                                oD.sourceTransactionId = m_b1.detail.orderData.sourceTransactionId;
                                oD.itemCode = it.itemcode;
                                oD.quantity = it.quantity;
                                if (!lsOrderDataDetail.Contains(oD))
                                    lsOrderDataDetail.Add(oD);
                                
                            }

                            foreach (var it in m_b1.detail.eCodes.Payload.data)
                            {
                                tiNiPassEcode oD = new tiNiPassEcode();
                                oD.id = m_b1.id;
                                oD.sourceTransactionId = m_b1.detail.orderData.sourceTransactionId;
                                oD.Ecode = it;
                                oD.status = m_b1.detail.eCodes.Payload.status;
                                if (!lstiNiPassEcode.Contains(oD))
                                    lstiNiPassEcode.Add(oD);
                            }

                        }
                        
                    //}


                    }
                   
                    WriteLog("Delete CRMData Database");
                    DeleteDataCRM();
                    WriteLog("Start Insert POSInfo");
                    var tasks1 = Task.Factory.StartNew(() => InsertPOSInfo());
                    WriteLog("Start Insert TiniPassEcode");
                    var tasks2 = Task.Factory.StartNew(() => InsertTiniPassEcode());
                    WriteLog("Start Insert OrderDataDetail");
                    var tasks3 = Task.Factory.StartNew(() => InsertOrderDataDetail());

                    WriteLog("Start Insert OrderDataHeader");
                    var tasks4 = Task.Factory.StartNew(() => InsertOrderDataHeader());

                    WriteLog("Start Insert CRMData");
                    var tasks5 = Task.Factory.StartNew(() => InsertCRMData());
                   
                    return true;
                }
                return true;
            }
            catch (Exception ex)
            {
                WriteLog("Error Insert CRMData Json "+ex.Message.ToString());
                return false;
            }
            

        }

        class AWS_Info
        {
            public string FileName { get; set; }
            public long FileSize { get; set; }

            public DateTime LastChange { get; set; }
        }
        private  async void button1_Click(object sender, EventArgs e)
        {
          
           //await InsertCRMDataJson();

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            RerunJobAllTiNiPass_Transactions();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            SendMailTrendingRevenueReport();
        }

        private void salesReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendMailSalesReport();
        }

        private void tiniPassSalesReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendMailTiniPassSalesReport();
        }

        private void billAndTrafficReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendMailBillAndTrafficReport();
        }

        private void volumeAnalyticsReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendMailVolumeAnalyticsReport();
        }

        private void topUpCampaignReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendMailTopUpCampaignReport();
        }

        private void marketingOnlineSaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendMailMarketingOnlineSaleReport();
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }
    }
}
