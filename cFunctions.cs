﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesReport
{
    public class cFunctions : cBaseData
    {
        public static string conStr;
        #region ============== Instance Process ==================
        public cFunctions() : base(conStr)
        {
        }

        public static cFunctions Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        public class Nested
        {
            //static Nested() { }
            internal static readonly cFunctions instance = new cFunctions();
        }

        #endregion

        public DataTable Select(string query)
        {
            DataTable dt = new DataTable();
            try
            {
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                var rs = new SqlDataAdapter(query, conn);
                rs.SelectCommand.CommandTimeout = 99999;
                rs.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw cGenFunctions.ConvertSQLException(ex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataSet ExcuteProc(string strProcName, Hashtable Params)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                
                cmd = new SqlCommand(strProcName, conn);
                foreach (DictionaryEntry item in Params)
                {
                    cmd.Parameters.Add(new SqlParameter(item.Key.ToString(), item.Value));
                }
                cmd.CommandType = CommandType.StoredProcedure;
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                cmd.CommandTimeout = 99999;
                dataAdapter.SelectCommand = cmd;

                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                dt.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw cGenFunctions.ConvertSQLException(ex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }


        public int GetValue(string strProcName, Hashtable Params)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                cmd = new SqlCommand(strProcName, conn);
                foreach (DictionaryEntry item in Params)
                {
                    cmd.Parameters.Add(new SqlParameter(item.Key.ToString(), item.Value));
                }
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 99999;
                return int.Parse( cmd.ExecuteScalar().ToString());
               
            }
            catch (SqlException ex)
            {
                throw cGenFunctions.ConvertSQLException(ex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return -1;
        }



        public void ExecProc(string strProcName)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                cmd = new SqlCommand(strProcName, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 99999;
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw cGenFunctions.ConvertSQLException(ex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertData(string strProcName, Hashtable Params)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                cmd = new SqlCommand(strProcName, conn);
                foreach (DictionaryEntry item in Params)
                {
                    cmd.Parameters.AddWithValue(item.Key.ToString(), item.Value);
                }
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 99999;
                int k = cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw cGenFunctions.ConvertSQLException(ex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
