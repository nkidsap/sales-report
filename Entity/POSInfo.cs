﻿using System;
using System.Collections.Generic;

using System.Text;

namespace SalesReport.Entity
{
    public class POSInfo
    {
      
        public string id { get; set; }
        public string username { get; set; }
        public string role { get; set; }
        public string storecode { get; set; }
        public string fullname { get; set; }
        public string type { get; set; }

    }
}
