﻿using System;
using System.Collections.Generic;

using System.Text;

namespace SalesReport.Entity
{
    public class OrderDataDetails
    {
       
        public string id { get; set; }
        public string sourceTransactionId { get; set; }
        public string itemCode { get; set; }
        public int quantity { get; set; }
      
    }
}
