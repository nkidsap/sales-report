﻿using MoralesLarios.Data.Dapper;
using SalesReport.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace SalesReport.Class
{
    public class POSInfoRepository : DPGenericRepository<POSInfo>
    {
        public POSInfoRepository(IDbConnection conn, char parameterIdentified = '@') : base(conn, parameterIdentified)
        {

        }

    }
}
