﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesReport.Class
{
    public  class RefreshData
    {
        public DataTable SourceData { get; set; }
        public string SheetName { get; set; }
        public int StartColumn { get; set; } = 1;
        public int StartRow { get; set; } = 1;
    }
}
