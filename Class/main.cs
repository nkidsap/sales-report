﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesReport
{
    public class base_main 
    {
        public string id { get; set; }
        public string detailtype { get; set; }
        public string account { get; set; }
        public DateTime time { get; set; }
        public string region { get; set; }
        public main detail { get; set; } = new main();
    }
    public class base_main_new
    {
        public string id { get; set; }
        public string detailtype { get; set; }
        public string account { get; set; }
        public DateTime time { get; set; }
        public string region { get; set; }
        public main_new detail { get; set; } = new main_new();
    }

    public class main
    {

        public sourceSystem sourceSystem { get; set; } = new sourceSystem();
        public orderData orderData { get; set; } = new orderData();
        public eCodes eCodes { get; set; } = new eCodes();
    }
    public class main_new
    {

        public sourceSystem sourceSystem { get; set; } = new sourceSystem();
        public orderData orderData { get; set; } = new orderData();
        public eCodesNew eCodes { get; set; } = new eCodesNew();
    }
    public class sourceSystem
    {
        public string name { get; set; }
        public posInfo posInfo { get; set; } = new posInfo();
        
    }

    public class orderData
    {
        public string sourceTransactionId { get; set; }
        
        public DateTime  orderDate { get; set; }
       
        public List<item> items { get; set; } = new List<item>();
        public shipTo shipTo { get; set; } = new shipTo();
    }
    public class item
    {
        public string itemcode { get; set; }
        public string sourceItemId { get; set; }
        public int quantity { get; set; }
    }

    public class eCodes
    {
        public Payload Payload { get; set; } = new Payload();
    }
    public class eCodesNew
    {
        public Payload_New Payload { get; set; } = new Payload_New();
    }
    public class Payload
    {
        public List<string> data { get; set; } = new List<string>();
        public string status { get; set; }
    }
    public class Payload_New
    {
        public List<TP_600> data { get; set; } = new List<TP_600>();
        public string status { get; set; }
    }
    public class TP_600
    {
        public List<string> TP600 { get; set; } = new List<string>();
    }
    public class shipTo
    {
        public string nfcCardNumber { get; set; }
        public string nfcCardType { get; set; }
        public string tinizenCustomerId { get; set; }
        public string name { get; set; }
        public string phoneNumber { get; set; }
        public bool sendEmail { get; set; }
    }
    public class posInfo
    {
        public string username { get; set; }
        public string role { get; set; }
        public string store_code { get; set; }
        public string fullname { get; set; }
        public string type { get; set; }
    }
}
