﻿using System;
using System.Collections.Generic;

using System.Text;

namespace SalesReport.Entity
{
    public class OrderDataHeader
    {
       
        public string id { get; set; }
        public string sourceTransactionId { get; set; }
        public DateTime orderDate { get; set; }
        public string nfcCardNumber { get; set; }
        public string nfcCardType { get; set; }
        public string tinizenCustomerId { get; set; }
        public string customername { get; set; }
        public string phoneNumber { get; set; }
        public bool sendEmail { get; set; }
    }
}
