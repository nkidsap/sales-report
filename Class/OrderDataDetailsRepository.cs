﻿using MoralesLarios.Data.Dapper;
using SalesReport.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace SalesReport.Class
{
    public class OrderDataDetailsRepository : DPGenericRepository<OrderDataDetails>
    {
        public OrderDataDetailsRepository(IDbConnection conn, char parameterIdentified = '@') : base(conn, parameterIdentified)
        {

        }

    }
}
