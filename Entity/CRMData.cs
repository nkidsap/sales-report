﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesReport.Entity
{
    public class CRMData
    {
       
        public string id { get; set; }
        public string detailtype { get; set; }
        public string account { get; set; }
        public DateTime time { get; set; }
        public string region { get; set; }

    }
}
