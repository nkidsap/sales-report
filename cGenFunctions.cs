﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SalesReport
{
    public class cGenFunctions
    {
        public static bool CheckIsInt(string Value)
        {
            try
            {
                int.Parse(Value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool CheckIsDouble(string Value)
        {
            try
            {
                int.Parse(Value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string UppercaseWords(string value)
        {
            char[] array = value.ToCharArray();
            // Handle the first letter in the string.
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
            // Scan through the letters, checking for spaces.
            // ... Uppercase the lowercase letters following spaces.
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == ' ')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }

        #region Convert DT To List

        public static List<T> ConvertTo<T>(DataTable datatable) where T : new()
        {
            List<T> Temp = new List<T>();
            try
            {
                List<string> columnsNames = new List<string>();
                foreach (DataColumn DataColumn in datatable.Columns)
                    columnsNames.Add(DataColumn.ColumnName);
                Temp = datatable.AsEnumerable().ToList().ConvertAll<T>(row => getObject<T>(row, columnsNames));
                return Temp;
            }
            catch
            {
                return Temp;
            }

        }
        public static T getObject<T>(DataRow row, List<string> columnsName) where T : new()
        {
            T obj = new T();
            try
            {
                string columnname = "";
                string value = "";
                PropertyInfo[] Properties;
                Properties = typeof(T).GetProperties();
                foreach (PropertyInfo objProperty in Properties)
                {
                    columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
                    if (!string.IsNullOrEmpty(columnname))
                    {
                        value = row[columnname].ToString();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
                            {
                                value = row[columnname].ToString().Replace("$", "").Replace(",", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(Nullable.GetUnderlyingType(objProperty.PropertyType).ToString())), null);
                            }
                            else
                            {
                                value = row[columnname].ToString().Replace("%", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(objProperty.PropertyType.ToString())), null);
                            }
                        }
                    }
                }
                return obj;
            }
            catch
            {
                return obj;
            }
        }
        #endregion

        public static Exception ConvertSQLException(SqlException ex)
        {
            Exception systemEx = null;
            switch (ex.Number)
            {
                case 53:
                    systemEx = new Exception("IP server không đúng. Vui lòng kiểm tra cấu hình.");
                    break;
                case 102:
                    systemEx = new Exception(string.Format("Lỗi cú pháp {0}", ex.Message));
                    break;
                case 207:
                    systemEx = new Exception(string.Format("Thiếu cột {0}", ex.Message));
                    break;
                case 208:
                    systemEx = new Exception(string.Format("Thiếu bảng {0}", ex.Message));
                    break;
                case 1326:
                    systemEx = new Exception("IP server không đúng. Vui lòng kiểm tra cấu hình.");
                    break;
                case 4060: // Invalid Database 
                    systemEx = new Exception("Tên database không đúng. Vui lòng kiểm tra cấu hình.");
                    break;
                case 18456: // Login Failed 
                    systemEx = new Exception("User/Password đăng nhập server không đúng. Vui lòng kiểm tra cấu hình.");
                    break;
                case 547: // ForeignKey Violation 
                    systemEx = new Exception("Không thể kết nối server. Vui lòng kiểm tra cấu hình.");
                    break;
                case 2627:
                    // Unique Index/ Primary key Violation/ Constriant Violation 
                    systemEx = new Exception("Không thể kết nối server. Vui lòng kiểm tra cấu hình.");
                    break;
                case 2601: // Unique Index/Constriant Violation 
                    systemEx = new Exception("Không thể kết nối server. Vui lòng kiểm tra cấu hình.");
                    break;
                default:
                    systemEx = new Exception("[Không thể kết nối server]" + ex.Message);
                    break;
            }
            return systemEx;
        }


    }
}
