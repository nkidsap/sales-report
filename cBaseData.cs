﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesReport
{
    public class cBaseData
    {
        //public PosModelDataContext dbcontext { get; set; }

        public SqlConnection conn;

        public cBaseData(string conStr)
        {
            try
            {
                //string conStr = "Data Source=210.211.118.3;Initial Catalog=POSONE_SERVER_LIVE;Persist Security Info=True;User ID=sa;Password=TiMj-2G~@G0\\__H";
                //dbcontext = new DataAccessDataContext(conStr);
                //if (dbcontext.Connection.State != System.Data.ConnectionState.Open)
                //    dbcontext.Connection.Open();
                conn = new SqlConnection(conStr);
                conn.Open();
            }
            catch (SqlException ex)
            {
                throw cGenFunctions.ConvertSQLException(ex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
